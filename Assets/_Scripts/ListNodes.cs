public static class ListNodes
{
    // List nodes of level 1
    public static class Lv1
    {
        public static Node node1 = new Node(-1, (float)0.36, 0);
        public static Node node2 = new Node((float)2.53, (float)0.36, 0);
        public static Node node3 = new Node((float)6.15, (float)0.36, 0);
    }

    // List nodes of level 2
    public static class Lv2
    {
        public static Node node1 = new Node(-6, 3, 0);
        public static Node node2 = new Node(-6, 0, 0);
        public static Node node3 = new Node(-6, -3, 0);
        public static Node node4 = new Node(-2, -3, 0);
        public static Node node5 = new Node(2, -3, 0);
        public static Node node6 = new Node(6, -3, 0);
    }

    // List nodes of level 3
    public static class Lv3
    {
        public static Node node1 = new Node(-2, 3, 0);
        public static Node node2 = new Node(-6, (float)0.4, 0);
        public static Node node3 = new Node(-2, (float)0.4, 0);
        public static Node node4 = new Node(2, (float)0.4, 0);
        public static Node node5 = new Node(6, (float)0.4, 0);
        public static Node node6 = new Node(-6, -3, 0);
        public static Node node7 = new Node(-2, -3, 0);
        public static Node node8 = new Node(2, -3, 0);
    }

    // List nodes of level 4
    public static class Lv4
    {
        public static Node node1 = new Node(-6, 4, 0);
        public static Node node2 = new Node((float)-2.7, 4, 0);
        public static Node node3 = new Node((float)1.167, 4, 0);
        public static Node node4 = new Node(4, 4, 0);
        public static Node node5 = new Node((float)-6.16, 2, 0);
        public static Node node6 = new Node((float)-2.908, 2, 0);
        public static Node node7 = new Node(1, 2, 0);
        public static Node node8 = new Node((float)3.84, 2, 0);
        public static Node node9 = new Node((float)6.76, 2, 0);
        public static Node node10 = new Node((float)-6.43, -1, 0);
        public static Node node11 = new Node((float)-3.17, -1, 0);
        public static Node node12 = new Node((float)0.73, -1, 0);
        public static Node node13 = new Node((float)3.58, -1, 0);
        public static Node node14 = new Node((float)6.5, -1, 0);
        public static Node node15 = new Node((float)-6.68, -4, 0);
        public static Node node16 = new Node((float)-3.43, -4, 0);
        public static Node node17 = new Node((float)0.48, -4, 0);
        public static Node node18 = new Node((float)3.3, -4, 0);
    }

    // List nodes of level 5
    public static class Lv5
    {
        public static Node node1 = new Node((float)-6.3, 4, 0);
        public static Node node2 = new Node((float)-2.6, 4, 0);
        public static Node node3 = new Node((float)0.73, 4, 0);
        public static Node node4 = new Node(4, 4, 0);
        public static Node node5 = new Node((float)-2.6, (float)2.2, 0);
        public static Node node6 = new Node((float)0.74, (float)2.2, 0);
        public static Node node7 = new Node(4, (float)2.2, 0);
        public static Node node8 = new Node((float)-6.3, (float)0.5, 0);
        public static Node node9 = new Node((float)-2.6, (float)0.5, 0);
        public static Node node10 = new Node((float)0.73, (float)0.5, 0);
        public static Node node11 = new Node(4, (float)0.5, 0);
        public static Node node12 = new Node((float)7.31, (float)0.5, 0);
        public static Node node13 = new Node((float)-2.6, (float)-1.77, 0);
        public static Node node14 = new Node((float)0.73, (float)-1.77, 0);
        public static Node node15 = new Node(4, (float)-1.77, 0);
        public static Node node16 = new Node((float)-2.6, -4, 0);
        public static Node node17 = new Node((float)0.73, -4, 0);
        public static Node node18 = new Node(4, -4, 0);
    }

    // List nodes of level 6
    public static class Lv6
    {
        public static Node node1 = new Node(-3, 3, 0);
        public static Node node2 = new Node(0, 3, 0);
        public static Node node3 = new Node(3, 3, 0);
        public static Node node4 = new Node(-6, 1, 0);
        public static Node node5 = new Node(-3, 1, 0);
        public static Node node6 = new Node(0, 1, 0);
        public static Node node7 = new Node(3, 1, 0);
        public static Node node8 = new Node(5, 1, 0);
        public static Node node9 = new Node(-6, -1, 0);
        public static Node node10 = new Node(-3, -1, 0);
        public static Node node11 = new Node(0, -1, 0);
        public static Node node12 = new Node(3, -1, 0);
        public static Node node13 = new Node((float) 5.25, -1, 0);
        public static Node node14 = new Node(-6, (float) -3.5, 0);
        public static Node node15 = new Node(-3, (float) -3.5, 0);
        public static Node node16 = new Node(0, (float) -3.5, 0);
        public static Node node17 = new Node(3, (float) -3.5, 0);
    }

    // List nodes of level 7
    public static class Lv7
    {
        public static Node node1 = new Node(6, (float) 3.3, 0);
        public static Node node2 = new Node((float) -5.4, (float) 1.3, 0);
        public static Node node3 = new Node((float) -2.7, (float) 1.3, 0);
        public static Node node4 = new Node(0, (float) 1.3, 0);
        public static Node node5 = new Node((float) 3.3, (float) 1.3, 0);
        public static Node node6 = new Node(6, (float) 1.3, 0);
        public static Node node7 = new Node((float) -5.6, -1, 0);
        public static Node node8 = new Node((float) -2.7, -1, 0);
        public static Node node9 = new Node((float) 0.2, -1, 0);
        public static Node node10 = new Node((float) 3.3, -1, 0);
        public static Node node11 = new Node(6, -1, 0);
        public static Node node12 = new Node(-6, (float) -3.2, 0);
        public static Node node13 = new Node((float) -2.7, (float) -3.2, 0);
        public static Node node14 = new Node((float) 0.2, (float) -3.2, 0);
        public static Node node15 = new Node(6, (float) -3.2, 0);
    }

    // List nodes of level 8
    public static class Lv8
    {
        public static Node node1 = new Node((float) -2.5, 4, 0);
        public static Node node2 = new Node(0, 4, 0);
        public static Node node3 = new Node(2, 4, 0);
        public static Node node4 = new Node(4, 4, 0);
        public static Node node5 = new Node((float) -2.5, (float) 2.5, 0);
        public static Node node6 = new Node(0, (float) 2.5, 0);
        public static Node node7 = new Node(2, (float) 2.5, 0);
        public static Node node8 = new Node(4, (float) 2.5, 0);
        public static Node node9 = new Node((float) -4.5, (float) 0.5, 0);
        public static Node node10 = new Node((float) -2.5, (float) 0.5, 0);
        public static Node node11 = new Node(0, (float) 0.5, 0);
        public static Node node12 = new Node(2, (float) 0.5, 0);
        public static Node node13 = new Node(4, (float) 0.5, 0);
        public static Node node14 = new Node((float) -4.5, (float) -1.5, 0);
        public static Node node15 = new Node((float) -2.5, (float) -1.5, 0);
        public static Node node16 = new Node(0, (float) -1.5, 0);
        public static Node node17 = new Node(2, (float) -1.5, 0);
        public static Node node18 = new Node(4, (float) -1.5, 0);
        public static Node node19 = new Node((float) -4.5, (float) -3.5, 0);
        public static Node node20 = new Node((float) -2.5, (float) -3.5, 0);
        public static Node node21 = new Node(0, (float) -3.5, 0);
        public static Node node22 = new Node(2, (float) -3.5, 0);
        public static Node node23 = new Node(4, (float) -3.5, 0);
    }

    // List nodes of level 9
    public static class Lv9
    {
        public static Node node1 = new Node(-5, 4, 0);
        public static Node node2 = new Node(-3, 4, 0);
        public static Node node3 = new Node(-1, 4, 0);
        public static Node node4 = new Node(-5, (float) 2.3, 0);
        public static Node node5 = new Node(-3, (float) 2.3, 0);
        public static Node node6 = new Node(-1, (float) 2.3, 0);
        public static Node node7 = new Node(2, (float) 2.3, 0);
        public static Node node8 = new Node(5, (float) 2.3, 0);
        public static Node node9 = new Node(-3, (float) 0.5, 0);
        public static Node node10 = new Node(-1, (float) 0.5, 0);
        public static Node node11 = new Node(2, (float) 0.5, 0);
        public static Node node12 = new Node(5, (float) 0.5, 0);
        public static Node node13 = new Node(-5, -1, 0);
        public static Node node14 = new Node(-3, -1, 0);
        public static Node node15 = new Node(-1, -1, 0);
        public static Node node16 = new Node(2, -1, 0);
        public static Node node17 = new Node(5, -1, 0);
        public static Node node18 = new Node(-5, (float) -3.5, 0);
        public static Node node19 = new Node(-3, (float) -3.5, 0);
        public static Node node20 = new Node(-1, (float) -3.5, 0);
        public static Node node21 = new Node(2, (float) -3.5, 0);
    }

    // List nodes of level 10
    public static class Lv10
    {
        public static Node node1 = new Node((float) -0.5, 4, 0);
        public static Node node2 = new Node((float) 1.5, 4, 0);
        public static Node node3 = new Node(-5, (float) 2.5, 0);
        public static Node node4 = new Node((float) -2.5, (float) 2.5, 0);
        public static Node node5 = new Node((float) -0.5, (float) 2.5, 0);
        public static Node node6 = new Node((float) 1.5, (float) 2.5, 0);
        public static Node node7 = new Node(4, (float) 2.5, 0);
        public static Node node8 = new Node(-5, 1, 0);
        public static Node node9 = new Node((float) -2.5, 1, 0);
        public static Node node10 = new Node((float) -0.5, 1, 0);
        public static Node node11 = new Node((float) 1.5, 1, 0);
        public static Node node12 = new Node(4, 1, 0);
        public static Node node13 = new Node(-5, -1, 0);
        public static Node node14 = new Node((float) -2.5, -1, 0);
        public static Node node15 = new Node((float) -0.5, -1, 0);
        public static Node node16 = new Node((float) 1.5, -1, 0);
        public static Node node17 = new Node(4, -1, 0);
        public static Node node18 = new Node((float) -2.5, -3, 0);
        public static Node node19 = new Node((float) 1.5, -3, 0);
    }

    // List nodes of level 11
    public static class Lv11
    {
        public static Node node1 = new Node((float) -5.5, 4, 0);
        public static Node node2 = new Node((float) 2.4, 4, 0);
        public static Node node3 = new Node((float) -5.5, 2, 0);
        public static Node node4 = new Node((float) -2.7, 2, 0);
        public static Node node5 = new Node(0, 2, 0);
        public static Node node6 = new Node((float) 2.4, 2, 0);
        public static Node node7 = new Node(5, 2, 0);
        public static Node node8 = new Node((float) -5.5, -1, 0);
        public static Node node9 = new Node((float) -2.7, -1, 0);
        public static Node node10 = new Node(0, -1, 0);
        public static Node node11 = new Node((float) 2.4, -1, 0);
        public static Node node12 = new Node(5, -1, 0);
        public static Node node13 = new Node((float) -5.5, (float) -3.5, 0);
        public static Node node14 = new Node((float) -2.7, (float) -3.5, 0);
        public static Node node15 = new Node(0, (float) -3.5, 0);
        public static Node node16 = new Node((float) 2.4, (float) -3.5, 0);
        public static Node node17 = new Node(5, (float) -3.5, 0);
    }

    // List nodes of level 12
    public static class Lv12
    {
        public static Node node1 = new Node(-6, 4, 0);
        public static Node node2 = new Node((float) -3.5, 4, 0);
        public static Node node3 = new Node((float) 2.5, 4, 0);
        public static Node node4 = new Node(5, 4, 0);
        public static Node node5 = new Node(-6, (float) 2.5, 0);
        public static Node node6 = new Node((float) -3.5, (float) 2.5, 0);
        public static Node node7 = new Node(0, (float) 2.5, 0);
        public static Node node8 = new Node((float) 2.5, (float) 2.5, 0);
        public static Node node9 = new Node((float) 5.5, (float) 2.5, 0);
        public static Node node10 = new Node((float) -3.5, (float) 0.5, 0);
        public static Node node11 = new Node(0, (float) 0.5, 0);
        public static Node node12 = new Node((float) 2.5, (float) 0.5, 0);
        public static Node node13 = new Node((float) 5.5, (float) 0.5, 0);
        public static Node node14 = new Node(-6, (float) -1.5, 0);
        public static Node node15 = new Node((float) -3.5, (float) -1.5, 0);
        public static Node node16 = new Node(0, (float) -1.5, 0);
        public static Node node17 = new Node((float) 2.5, (float) -1.5, 0);
        public static Node node18 = new Node(-6, -4, 0);
        public static Node node19 = new Node((float) -3.5, -4, 0);
        public static Node node20 = new Node(0, -4, 0);
    }
}