using System.IO;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private GameObject SettingPanel;
    [SerializeField] private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        Data.CreateFile(Data.MutePath, "false");
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
    }

    public void PlayGameButton()
    {
        Data.CreateFile(Data.LevelPath, "0");
        Data.CreateFile(Data.ScorePath, "0");
        Data.CreateFile(Data.CriLev5Path, "");
        Data.CreateFile(Data.CriLev6Path, "");
        Data.CreateFile(Data.CriLev7Path, "");
        Data.CreateFile(Data.CriLev8Path, "");
        Data.CreateFile(Data.CriLev9Path, "");
        Data.CreateFile(Data.CriLev10Path, "");
        Data.CreateFile(Data.CriLev11Path, "");
        Data.CreateFile(Data.CriLev12Path, "");
        Application.LoadLevel("Choose Chapter");
    }

    public void QuitGameButton()
    {
        Application.Quit();
    }

    public void SettingGameButton()
    {
        SettingPanel.SetActive(true);
        Time.timeScale = 0f;
    }
}