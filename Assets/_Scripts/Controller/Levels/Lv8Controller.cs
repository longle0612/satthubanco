using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv8Controller : MonoBehaviour
{
    // Player and enemies
    private GameObject player;
    private GameObject st;
    private GameObject fl1;
    private GameObject fl2;
    private GameObject fl3;
    private Direction dirFl1;
    private Direction dirFl2;
    private Direction dirFl3;

    private GameObject suitCase;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject noKill;
    [SerializeField] private GameObject collectBriefcase;
    [SerializeField] private GameObject toChap2Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        suitCase = GameObject.Find("SuitCase");
        dirFl1 = Direction.Left;
        dirFl2 = Direction.Right;
        dirFl3 = Direction.Right;
        st = GameObject.Find("Stl");
        fl1 = GameObject.Find("Flru 1");
        fl2 = GameObject.Find("Flru 2");
        fl3 = GameObject.Find("Flru 3");
        player = GameObject.Find("Hi");
        Criterias.noKill = true;
        Criterias.isCollectBriefcase = false;
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        EnemiesMove();
        PlayerMove();
        switch (Data.ReadFile(Data.CriLev8Path))
        {
            case "12":
                noKill.SetActive(true);
                break;
            case "13":
                collectBriefcase.SetActive(true);
                break;
            case "123":
            case "132":
                noKill.SetActive(true);
                collectBriefcase.SetActive(true);
                break;
        }
    }

    void PlayerMove()
    {
        Vector3 pos = player.transform.position;

        if (Vector3ToIntNode(pos) == 1)
        {
            Destroy(suitCase);
            Criterias.isCollectBriefcase = true;
        }

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node3);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node2);
                        break;
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node1);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node5);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node6);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node7);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node11);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node9);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node10);
                        break;
                    case 13:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node12);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node16);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node14);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node15);
                        break;
                    case 21:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node20);
                        break;
                    case 22:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node21);
                        break;
                    case 20:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node19);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 1:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node2);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node4);
                        break;
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node3);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node7);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node8);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node6);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node13);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node11);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node12);
                        break;
                    case 14:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node15);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node16);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node17);
                        break;
                    case 21:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node22);
                        break;
                    case 19:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node20);
                        break;
                    case 20:
                        helpers.ChangePositionX(player, ListNodes.Lv8.node21);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node2);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node1);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node6);
                        break;
                    case 14:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node9);
                        break;
                    case 17:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node12);
                        break;
                    case 18:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node13);
                        break;
                    case 19:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node14);
                        break;
                    case 20:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node15);
                        break;
                    case 21:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node16);
                        break;
                    case 23:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node18);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 1:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node5);
                        break;
                    case 2:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node6);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node11);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node17);
                        break;
                    case 13:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node18);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node20);
                        break;
                    case 16:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node21);
                        break;
                    case 14:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node19);
                        break;
                    case 18:
                        helpers.ChangePositionY(player, ListNodes.Lv8.node23);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(pos) == 9)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 7)
            {
                File.AppendAllText(Data.CriLev8Path, "1");
                level++;
                score++;
                if (Criterias.noKill)
                {
                    File.AppendAllText(Data.CriLev8Path, "2");
                    score++;
                }

                if (Criterias.isCollectBriefcase)
                {
                    File.AppendAllText(Data.CriLev8Path, "3");
                    score++;
                }
            }
            else
            {
                switch (File.ReadAllText(Data.CriLev8Path))
                {
                    case "1":
                        if (Criterias.noKill)
                        {
                            File.AppendAllText(Data.CriLev8Path, "2");
                            score++;
                        }

                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev8Path, "3");
                            score++;
                        }

                        break;
                    case "12":
                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev8Path, "3");
                            score++;
                        }

                        break;
                    case "13":
                        if (Criterias.noKill)
                        {
                            File.AppendAllText(Data.CriLev8Path, "2");
                            score++;
                        }

                        break;
                }
            }

            File.WriteAllText(Data.LevelPath, level.ToString());
            File.WriteAllText(Data.ScorePath, score.ToString());
            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 8 COMPLETE";
        }
    }

    void EnemiesMove()
    {
        if (FlKillPlayer())
        {
            player.transform.position = new Vector3(-7, 3, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            Time.timeScale = 0f;
        }

        Vector3 playerPos = player.transform.position;
        switch (Vector3ToIntNode(playerPos))
        {
            case 1:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 2:
            case 12:
            case 15:
            case 16:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 3:
            case 7:
            case 10:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 4:
            case 8:
            case 22:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 5:
            case 19:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                    case "D":
                    case "RightArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 6:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 11:
            case 20:
            case 21:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 13:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 14:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 17:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 18:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 23:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
        }
    }

    void EnemiesMoveSteps()
    {
        Fl1MoveSteps();
        Fl2MoveSteps();
        Fl3MoveSteps();
    }

    void Fl1MoveSteps()
    {
        Vector3 fl1Pos = fl1.transform.position;
        Vector3 playerPos = player.transform.position;
        if (fl1Pos == new Vector3(-5, 3, 0))
        {
            Criterias.noKill = false;
            return;
        }

        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        bool isKeyD = Input.GetKeyDown(KeyCode.D);
        bool isKeyRight = Input.GetKeyDown(KeyCode.RightArrow);
        bool isKeyS = Input.GetKeyDown(KeyCode.S);
        bool isKeyDown = Input.GetKeyDown(KeyCode.DownArrow);
        switch (dirFl1)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 8:
                        helpers.FlChangePos(fl1, ListNodes.Lv8.node7);
                        break;
                    case 7:
                        if (Vector3ToIntNode(playerPos) == 6 && (isKeyD || isKeyRight))
                        {
                            fl1.transform.position = new Vector3(-5, 3, 0);
                            return;
                        }

                        helpers.FlChangePos(fl1, ListNodes.Lv8.node6);
                        break;
                    case 6:
                        if (Vector3ToIntNode(playerPos) == 11 && (isKeyW || isKeyUp))
                        {
                            fl1.transform.position = new Vector3(-5, 3, 0);
                            return;
                        }

                        if (Vector3ToIntNode(playerPos) == 5 && (isKeyD || isKeyRight))
                        {
                            fl1.transform.position = new Vector3(-5, 3, 0);
                            return;
                        }

                        if (Vector3ToIntNode(playerPos) == 2 && (isKeyS || isKeyDown))
                        {
                            fl1.transform.position = new Vector3(-5, 3, 0);
                            return;
                        }

                        helpers.FlChangePos(fl1, ListNodes.Lv8.node5);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl1 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 5:
                        if (Vector3ToIntNode(playerPos) == 1 && (isKeyS || isKeyDown))
                        {
                            fl1.transform.position = new Vector3(-5, 3, 0);
                            return;
                        }

                        helpers.FlChangePos(fl1, ListNodes.Lv8.node6);
                        break;
                    case 6:
                        if (Vector3ToIntNode(playerPos) == 11 && (isKeyW || isKeyUp))
                        {
                            fl1.transform.position = new Vector3(-5, 3, 0);
                            return;
                        }

                        if (Vector3ToIntNode(playerPos) == 2 && (isKeyS || isKeyDown))
                        {
                            fl1.transform.position = new Vector3(-5, 3, 0);
                            return;
                        }

                        helpers.FlChangePos(fl1, ListNodes.Lv8.node7);
                        break;
                    case 7:
                        helpers.FlChangePos(fl1, ListNodes.Lv8.node8);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl1 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl2MoveSteps()
    {
        Vector3 fl2Pos = fl2.transform.position;
        switch (dirFl2)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 13:
                        helpers.FlChangePos(fl2, ListNodes.Lv8.node12);
                        break;
                    case 12:
                        helpers.FlChangePos(fl2, ListNodes.Lv8.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl2, ListNodes.Lv8.node10);
                        break;
                    case 10:
                        helpers.FlChangePos(fl2, ListNodes.Lv8.node9);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl2 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 9:
                        helpers.FlChangePos(fl2, ListNodes.Lv8.node10);
                        break;
                    case 10:
                        helpers.FlChangePos(fl2, ListNodes.Lv8.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl2, ListNodes.Lv8.node12);
                        break;
                    case 12:
                        helpers.FlChangePos(fl2, ListNodes.Lv8.node13);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl2 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl3MoveSteps()
    {
        Vector3 fl3Pos = fl3.transform.position;
        switch (dirFl3)
        {
            case Direction.Right:
                switch (Vector3ToIntNode(fl3Pos))
                {
                    case 14:
                        helpers.FlChangePos(fl3, ListNodes.Lv8.node15);
                        break;
                    case 15:
                        helpers.FlChangePos(fl3, ListNodes.Lv8.node16);
                        break;
                    case 16:
                        helpers.FlChangePos(fl3, ListNodes.Lv8.node17);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl3 = Direction.Left;
                        break;
                }

                break;
            case Direction.Left:
                switch (Vector3ToIntNode(fl3Pos))
                {
                    case 17:
                        helpers.FlChangePos(fl3, ListNodes.Lv8.node16);
                        break;
                    case 16:
                        helpers.FlChangePos(fl3, ListNodes.Lv8.node15);
                        break;
                    case 15:
                        helpers.FlChangePos(fl3, ListNodes.Lv8.node14);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl3 = Direction.Right;
                        break;
                }

                break;
        }
    }

    bool FlKillPlayer()
    {
        if (fl1.transform.position == player.transform.position) return true;
        if (fl2.transform.position == player.transform.position) return true;
        if (fl3.transform.position == player.transform.position) return true;
        if (player.transform.position == helpers.CreateVector3(ListNodes.Lv8.node20))
        {
            st.transform.position = helpers.CreateVector3(ListNodes.Lv8.node20);
            return true;
        }

        return false;
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 8)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap2Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap2Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap2()
    {
        Application.LoadLevel("Chapter 2");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node8)) return 8;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node9)) return 9;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node10)) return 10;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node11)) return 11;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node12)) return 12;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node13)) return 13;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node14)) return 14;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node15)) return 15;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node16)) return 16;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node17)) return 17;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node18)) return 18;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node19)) return 19;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node20)) return 20;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node21)) return 21;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node22)) return 22;
        if (vector == helpers.CreateVector3(ListNodes.Lv8.node23)) return 23;
        return 0;
    }
}