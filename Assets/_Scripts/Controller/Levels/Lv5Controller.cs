using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv5Controller : MonoBehaviour
{
    // Player and enemies
    private GameObject player;
    private Direction dirFl1;
    private Direction dirFl2;
    private Direction dirFl3;
    private GameObject fl1;
    private GameObject fl2;
    private GameObject fl3;

    private GameObject suitCase;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject steps;
    [SerializeField] private GameObject collectBriefcase;
    [SerializeField] private GameObject toChap2Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        suitCase = GameObject.Find("SuitCase");
        dirFl1 = Direction.Up;
        dirFl2 = Direction.Up;
        dirFl3 = Direction.Down;
        fl1 = GameObject.Find("Flru1");
        fl2 = GameObject.Find("Flru2");
        fl3 = GameObject.Find("Flru3");
        player = GameObject.Find("Hi Lv5");
        Criterias.countSteps = 0;
        Criterias.isCollectBriefcase = false;
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        FlMove();
        PlayerMove();
        switch (Data.ReadFile(Data.CriLev5Path))
        {
            case "12":
                steps.SetActive(true);
                break;
            case "13":
                collectBriefcase.SetActive(true);
                break;
            case "123":
            case "132":
                steps.SetActive(true);
                collectBriefcase.SetActive(true);
                break;
        }
    }

    void PlayerMove()
    {
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 1)
        {
            Destroy(suitCase);
            Criterias.isCollectBriefcase = true;
        }

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node1);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node2);
                        break;
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node3);
                        break;
                    case 9:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node8);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node10);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node16);
                        break;
                    case 18:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node17);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 16:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node13);
                        break;
                    case 13:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node9);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node5);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node2);
                        break;
                    case 14:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node10);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node6);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node3);
                        break;
                    case 18:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node15);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node11);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node7);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node4);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 2:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node5);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node9);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node13);
                        break;
                    case 13:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node16);
                        break;
                    case 3:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node6);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node10);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node14);
                        break;
                    case 4:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node7);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node11);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node15);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv5.node18);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 1:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node2);
                        break;
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node3);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node4);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node9);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node11);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node12);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node17);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv5.node18);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(playerPos) == 12)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 4)
            {
                File.AppendAllText(Data.CriLev5Path, "1");
                level++;
                score++;
                if (Criterias.countSteps <= 14)
                {
                    File.AppendAllText(Data.CriLev5Path, "2");
                    score++;
                }

                if (Criterias.isCollectBriefcase)
                {
                    File.AppendAllText(Data.CriLev5Path, "3");
                    score++;
                }
            }
            else
            {
                switch (File.ReadAllText(Data.CriLev5Path))
                {
                    case "1":
                        if (Criterias.countSteps <= 14)
                        {
                            File.AppendAllText(Data.CriLev5Path, "2");
                            score++;
                        }

                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev5Path, "3");
                            score++;
                        }

                        break;
                    case "12":
                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev5Path, "3");
                            score++;
                        }

                        break;
                    case "13":
                        if (Criterias.countSteps <= 14)
                        {
                            File.AppendAllText(Data.CriLev5Path, "2");
                            score++;
                        }

                        break;
                }
            }

            File.WriteAllText(Data.LevelPath, level.ToString());
            File.WriteAllText(Data.ScorePath, score.ToString());
            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 5 COMPLETE";
        }
    }

    void FlMove()
    {
        if (EnemiesKillPlayer())
        {
            player.transform.position = new Vector3(-6, -1, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
        }

        Vector3 playerPos = player.transform.position;

        switch (Vector3ToIntNode(playerPos))
        {
            case 1:
            case 8:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 2:
            case 3:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                    case "D":
                    case "RightArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 4:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 5:
            case 6:
            case 7:
            case 13:
            case 15:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                    case "S":
                    case "DownArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 9:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 10:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 11:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                    case "W":
                    case "UpArrow":
                    case "D":
                    case "RightArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 14:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 16:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                    case "D":
                    case "RightArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 17:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 18:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
        }
    }

    void FlMoveSteps()
    {
        Fl1MoveSteps();
        Fl2MoveSteps();
        Fl3MoveSteps();
        Criterias.countSteps++;
    }

    void Fl1MoveSteps()
    {
        switch (dirFl1)
        {
            case Direction.Up:
                switch (Vector3ToIntNode(fl1.transform.position))
                {
                    case 16:
                        helpers.FlChangePos(fl1, ListNodes.Lv5.node13);
                        break;
                    case 13:
                        helpers.FlChangePos(fl1, ListNodes.Lv5.node9);
                        break;
                    case 9:
                        helpers.FlChangePos(fl1, ListNodes.Lv5.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl1, ListNodes.Lv5.node2);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, 180);
                        dirFl1 = Direction.Down;
                        break;
                }

                break;
            case Direction.Down:
                switch (Vector3ToIntNode(fl1.transform.position))
                {
                    case 2:
                        helpers.FlChangePos(fl1, ListNodes.Lv5.node5);
                        break;
                    case 13:
                        helpers.FlChangePos(fl1, ListNodes.Lv5.node16);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, 360);
                        dirFl1 = Direction.Up;
                        break;
                    case 9:
                        helpers.FlChangePos(fl1, ListNodes.Lv5.node13);
                        break;
                    case 5:
                        helpers.FlChangePos(fl1, ListNodes.Lv5.node9);
                        break;
                }

                break;
        }
    }

    void Fl2MoveSteps()
    {
        bool isKeyA = Input.GetKeyDown(KeyCode.A);
        bool isKeyLeft = Input.GetKeyDown(KeyCode.LeftArrow);
        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        Vector3 fl2Pos = fl2.transform.position;
        Vector3 playerPos = player.transform.position;

        switch (dirFl2)
        {
            case Direction.Up:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 14:
                        helpers.FlChangePos(fl2, ListNodes.Lv5.node10);
                        break;
                    case 10:
                        if (Vector3ToIntNode(playerPos) == 11 && (isKeyA || isKeyLeft))
                            fl2.transform.position = new Vector3(-6, -3, 0);
                        else helpers.FlChangePos(fl2, ListNodes.Lv5.node6);
                        break;
                    case 6:
                        helpers.FlChangePos(fl2, ListNodes.Lv5.node3);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, 180);
                        dirFl2 = Direction.Down;
                        break;
                }

                break;
            case Direction.Down:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 3:
                        helpers.FlChangePos(fl2, ListNodes.Lv5.node6);
                        break;
                    case 6:
                        if (Vector3ToIntNode(playerPos) == 10 && (isKeyW || isKeyUp))
                            fl2.transform.position = new Vector3(-6, -3, 0);
                        else helpers.FlChangePos(fl2, ListNodes.Lv5.node10);
                        break;
                    case 10:
                        if (Vector3ToIntNode(playerPos) == 11 && (isKeyA || isKeyLeft))
                            fl2.transform.position = new Vector3(-6, -3, 0);
                        else if (Vector3ToIntNode(playerPos) == 14 && (isKeyW || isKeyUp))
                            fl2.transform.position = new Vector3(-6, -3, 0);
                        else
                        {
                            helpers.FlChangePos(fl2, ListNodes.Lv5.node14);
                            fl2.transform.rotation = Quaternion.Euler(0, 0, 360);
                            dirFl2 = Direction.Up;
                        }

                        break;
                }

                break;
        }
    }

    void Fl3MoveSteps()
    {
        switch (dirFl3)
        {
            case Direction.Down:
                switch (Vector3ToIntNode(fl3.transform.position))
                {
                    case 4:
                        helpers.FlChangePos(fl3, ListNodes.Lv5.node7);
                        break;
                    case 7:
                        helpers.FlChangePos(fl3, ListNodes.Lv5.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl3, ListNodes.Lv5.node15);
                        break;
                    case 15:
                        helpers.FlChangePos(fl3, ListNodes.Lv5.node18);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, 180);
                        dirFl3 = Direction.Up;
                        break;
                }

                break;
            case Direction.Up:
                switch (Vector3ToIntNode(fl3.transform.position))
                {
                    case 18:
                        helpers.FlChangePos(fl3, ListNodes.Lv5.node15);
                        break;
                    case 7:
                        helpers.FlChangePos(fl3, ListNodes.Lv5.node4);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, 360);
                        dirFl3 = Direction.Down;
                        break;
                    case 11:
                        helpers.FlChangePos(fl3, ListNodes.Lv5.node7);
                        break;
                    case 15:
                        helpers.FlChangePos(fl3, ListNodes.Lv5.node11);
                        break;
                }

                break;
        }
    }

    bool EnemiesKillPlayer()
    {
        if (fl1.transform.position == player.transform.position) return true;
        if (fl2.transform.position == player.transform.position) return true;
        if (fl3.transform.position == player.transform.position) return true;
        return false;
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 5)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap2Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap2Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap2()
    {
        Application.LoadLevel("Chapter 2");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node8)) return 8;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node9)) return 9;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node10)) return 10;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node11)) return 11;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node12)) return 12;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node13)) return 13;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node14)) return 14;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node15)) return 15;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node16)) return 16;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node17)) return 17;
        if (vector == helpers.CreateVector3(ListNodes.Lv5.node18)) return 18;
        return 0;
    }
}