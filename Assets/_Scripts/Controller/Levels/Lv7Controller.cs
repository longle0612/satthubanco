using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv7Controller : MonoBehaviour
{
    // Player and enemies
    private GameObject player;
    private Direction dirFl1;
    private Direction dirFl2;
    private GameObject fl1;
    private GameObject fl2;

    private GameObject suitCase;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject steps;
    [SerializeField] private GameObject collectBriefcase;
    [SerializeField] private GameObject toChap2Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        suitCase = GameObject.Find("SuitCase");
        dirFl1 = Direction.Right;
        dirFl2 = Direction.Left;
        fl1 = GameObject.Find("Flru1");
        fl2 = GameObject.Find("Flru2");
        player = GameObject.Find("Hi Lv7");
        Criterias.countSteps = 0;
        Criterias.isCollectBriefcase = false;
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        FlMove();
        PlayerMove();
        switch (Data.ReadFile(Data.CriLev7Path))
        {
            case "12":
                steps.SetActive(true);
                break;
            case "13":
                collectBriefcase.SetActive(true);
                break;
            case "123":
            case "132":
                steps.SetActive(true);
                collectBriefcase.SetActive(true);
                break;
        }
    }

    void PlayerMove()
    {
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 1)
        {
            Destroy(suitCase);
            Criterias.isCollectBriefcase = true;
        }

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node2);
                        break;
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node3);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node4);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node5);
                        break;
                    case 9:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node8);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node9);
                        break;
                    case 14:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node13);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node1);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node2);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node5);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node6);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node7);
                        break;
                    case 13:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node8);
                        break;
                    case 14:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node9);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node11);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 1:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node6);
                        break;
                    case 2:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node7);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node10);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node11);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node12);
                        break;
                    case 8:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node13);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node14);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv7.node15);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node3);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node4);
                        break;
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node5);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node6);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node9);
                        break;
                    case 9:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node10);
                        break;
                    case 13:
                        helpers.ChangePositionX(player, ListNodes.Lv7.node14);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(playerPos) == 12)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 6)
            {
                File.AppendAllText(Data.CriLev7Path, "1");
                level++;
                score++;
                if (Criterias.countSteps <= 16)
                {
                    File.AppendAllText(Data.CriLev7Path, "2");
                    score++;
                }

                if (Criterias.isCollectBriefcase)
                {
                    File.AppendAllText(Data.CriLev7Path, "3");
                    score++;
                }
            }
            else
            {
                switch (File.ReadAllText(Data.CriLev7Path))
                {
                    case "1":
                        if (Criterias.countSteps <= 16)
                        {
                            File.AppendAllText(Data.CriLev7Path, "2");
                            score++;
                        }

                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev7Path, "3");
                            score++;
                        }

                        break;
                    case "12":
                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev7Path, "3");
                            score++;
                        }

                        break;
                    case "13":
                        if (Criterias.countSteps <= 16)
                        {
                            File.AppendAllText(Data.CriLev7Path, "2");
                            score++;
                        }

                        break;
                }
            }

            File.WriteAllText(Data.LevelPath, level.ToString());
            File.WriteAllText(Data.ScorePath, score.ToString());
            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 7 COMPLETE";
        }
    }

    void FlMove()
    {
        if (FlKillPlayer())
        {
            player.transform.position = new Vector3((float) 3.3, (float) -3.2, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            Time.timeScale = 0f;
        }

        Vector3 playerPos = player.transform.position;

        switch (Vector3ToIntNode(playerPos))
        {
            case 1:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 2:
            case 8:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 3:
            case 4:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 5:
            case 9:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 6:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                    case "S":
                    case "DownArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 7:
            case 11:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                    case "S":
                    case "DownArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 10:
            case 14:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 12:
            case 15:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 13:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
        }
    }

    void FlMoveSteps()
    {
        Fl1MoveSteps();
        Fl2MoveSteps();
        Criterias.countSteps++;
    }

    void Fl1MoveSteps()
    {
        Vector3 fl1Pos = fl1.transform.position;
        switch (dirFl1)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 10:
                        helpers.FlChangePos(fl1, ListNodes.Lv7.node9);
                        break;
                    case 9:
                        helpers.FlChangePos(fl1, ListNodes.Lv7.node8);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl1 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 8:
                        helpers.FlChangePos(fl1, ListNodes.Lv7.node9);
                        break;
                    case 9:
                        helpers.FlChangePos(fl1, ListNodes.Lv7.node10);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl1 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl2MoveSteps()
    {
        Vector3 fl2Pos = fl2.transform.position;
        switch (dirFl2)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 6:
                        helpers.FlChangePos(fl2, ListNodes.Lv7.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl2, ListNodes.Lv7.node4);
                        break;
                    case 4:
                        helpers.FlChangePos(fl2, ListNodes.Lv7.node3);
                        break;
                    case 3:
                        helpers.FlChangePos(fl2, ListNodes.Lv7.node2);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl2 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 2:
                        helpers.FlChangePos(fl2, ListNodes.Lv7.node3);
                        break;
                    case 3:
                        helpers.FlChangePos(fl2, ListNodes.Lv7.node4);
                        break;
                    case 4:
                        helpers.FlChangePos(fl2, ListNodes.Lv7.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl2, ListNodes.Lv7.node6);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl2 = Direction.Left;
                        break;
                }

                break;
        }
    }

    bool FlKillPlayer()
    {
        if (fl1.transform.position == player.transform.position &&
            player.transform.position != helpers.CreateVector3(ListNodes.Lv7.node9)) return true;
        if (fl2.transform.position == player.transform.position &&
            player.transform.position != helpers.CreateVector3(ListNodes.Lv7.node3)) return true;
        return false;
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 7)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap2Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap2Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap2()
    {
        Application.LoadLevel("Chapter 2");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node8)) return 8;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node9)) return 9;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node10)) return 10;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node11)) return 11;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node12)) return 12;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node13)) return 13;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node14)) return 14;
        if (vector == helpers.CreateVector3(ListNodes.Lv7.node15)) return 15;
        return 0;
    }
}