using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv12Controller : MonoBehaviour
{
    // player and enemies
    private GameObject player;
    private GameObject tu1;
    private GameObject tu2;
    private GameObject tu3;
    private GameObject tu4;
    private GameObject tu5;
    private GameObject tu6;

    // Buttons
    private Button sw1Button;
    private Button sw2Button;
    private Button sw3Button;
    private Button sw4Button;

    // Directions of enemies
    private Direction dirTu1;
    private Direction dirTu2;
    private Direction dirTu3;
    private Direction dirTu4;
    private Direction dirTu5;
    private Direction dirTu6;

    // items
    private GameObject suitCase;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject steps;
    [SerializeField] private GameObject collectBriefcase;
    [SerializeField] private GameObject toChap3Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        suitCase = GameObject.Find("SuitCase");
        tu1 = GameObject.Find("Tuu 1");
        tu2 = GameObject.Find("Tuu 2");
        tu3 = GameObject.Find("Tuu 3");
        tu4 = GameObject.Find("Tuu 4");
        tu5 = GameObject.Find("Tuu 5");
        tu6 = GameObject.Find("Tuu 6");
        dirTu1 = Direction.Left;
        dirTu2 = Direction.Left;
        dirTu3 = Direction.Right;
        dirTu4 = Direction.Right;
        dirTu5 = Direction.Left;
        dirTu6 = Direction.Right;
        player = GameObject.Find("Hi");

        sw1Button = GameObject.Find("Sw1 Button").GetComponent<Button>();
        sw2Button = GameObject.Find("Sw2 Button").GetComponent<Button>();
        sw3Button = GameObject.Find("Sw3 Button").GetComponent<Button>();
        sw4Button = GameObject.Find("Sw4 Button").GetComponent<Button>();

        Criterias.countSteps = 0;
        Criterias.isCollectBriefcase = false;
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        EnemiesMove();
        PlayerMove();
        switch (Data.ReadFile(Data.CriLev12Path))
        {
            case "12":
                steps.SetActive(true);
                break;
            case "13":
                collectBriefcase.SetActive(true);
                break;
            case "123":
            case "132":
                steps.SetActive(true);
                collectBriefcase.SetActive(true);
                break;
        }
    }

    void PlayerMove()
    {
        Vector3 playerPos = player.transform.position;

        showButton(sw1Button, ListNodes.Lv12.node7);
        showButton(sw2Button, ListNodes.Lv12.node6);
        showButton(sw3Button, ListNodes.Lv12.node12);
        showButton(sw4Button, ListNodes.Lv12.node10);

        if (Vector3ToIntNode(playerPos) == 2)
        {
            Destroy(suitCase);
            Criterias.isCollectBriefcase = true;
        }

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node1);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node5);
                        break;
                    case 9:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node8);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node7);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node11);
                        break;
                    case 13:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node12);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node14);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node15);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node16);
                        break;
                    case 19:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node18);
                        break;
                    case 20:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node19);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 1:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node2);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node4);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node6);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node9);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node8);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node13);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node12);
                        break;
                    case 14:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node15);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node16);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node17);
                        break;
                    case 18:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node19);
                        break;
                    case 19:
                        helpers.ChangePositionX(player, ListNodes.Lv12.node20);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node1);
                        break;
                    case 8:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node3);
                        break;
                    case 13:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node9);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node7);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node10);
                        break;
                    case 17:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node12);
                        break;
                    case 20:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node16);
                        break;
                    case 18:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node14);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 3:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node8);
                        break;
                    case 1:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node5);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node11);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node13);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node15);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node17);
                        break;
                    case 14:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node18);
                        break;
                    case 16:
                        helpers.ChangePositionY(player, ListNodes.Lv12.node20);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(playerPos) == 4)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 11)
            {
                File.AppendAllText(Data.CriLev12Path, "1");
                level++;
                score++;
                if (Criterias.countSteps <= 17)
                {
                    File.AppendAllText(Data.CriLev12Path, "2");
                    score++;
                }

                if (Criterias.isCollectBriefcase)
                {
                    File.AppendAllText(Data.CriLev12Path, "3");
                    score++;
                }
            }
            else
            {
                switch (File.ReadAllText(Data.CriLev12Path))
                {
                    case "1":
                        if (Criterias.countSteps <= 17)
                        {
                            File.AppendAllText(Data.CriLev12Path, "2");
                            score++;
                        }

                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev12Path, "3");
                            score++;
                        }

                        break;
                    case "12":
                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev12Path, "3");
                            score++;
                        }

                        break;
                    case "13":
                        if (Criterias.countSteps <= 17)
                        {
                            File.AppendAllText(Data.CriLev12Path, "2");
                            score++;
                        }

                        break;
                }
            }

            File.WriteAllText(Data.LevelPath, level.ToString());
            File.WriteAllText(Data.ScorePath, score.ToString());
            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 12 COMPLETE";
        }
    }

    void EnemiesMove()
    {
        Vector3 playerPos = player.transform.position;
        switch (Vector3ToIntNode(playerPos))
        {
            case 1:
            case 3:
            case 7:
            case 14:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 2:
            case 6:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 5:
            case 11:
            case 18:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 8:
            case 15:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 9:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 10:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 12:
            case 16:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 13:
            case 17:
            case 20:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 19:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
        }
    }

    void EnemiesMoveSteps()
    {
        Tuu1MoveSteps();
        Tuu2MoveSteps();
        Tuu3MoveSteps();
        Tuu4MoveSteps();
        Tuu5MoveSteps();
        Tuu6MoveSteps();
        Criterias.countSteps++;
    }

    void Tuu1MoveSteps()
    {
        if (Vector3ToIntNode(tu1.transform.position) != 2) return;

        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        bool isKeyD = Input.GetKeyDown(KeyCode.D);
        bool isKeyRight = Input.GetKeyDown(KeyCode.RightArrow);
        Vector3 playerPos = player.transform.position;

        switch (dirTu1)
        {
            case Direction.Left:
                if (Vector3ToIntNode(playerPos) == 5 && (isKeyW || isKeyUp))
                {
                    tu1.transform.position = helpers.CreateVector3(ListNodes.Lv12.node1);
                    player.transform.position = new Vector3(3, -4, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                if (Vector3ToIntNode(playerPos) == 1 && (isKeyD || isKeyRight))
                {
                    tu1.transform.position = new Vector3((float) -7.5, 4, 0);
                    return;
                }

                tu1.transform.rotation = Quaternion.Euler(0, 0, -90);
                dirTu1 = Direction.Right;
                break;
            case Direction.Right:
                tu1.transform.rotation = Quaternion.Euler(0, 0, 90);
                dirTu1 = Direction.Left;
                break;
        }
    }

    void Tuu2MoveSteps()
    {
        if (Vector3ToIntNode(tu2.transform.position) != 5) return;

        bool isKeyA = Input.GetKeyDown(KeyCode.A);
        bool isKeyLeft = Input.GetKeyDown(KeyCode.LeftArrow);

        if (Vector3ToIntNode(player.transform.position) == 6 && (isKeyA || isKeyLeft))
        {
            tu2.transform.position = new Vector3(-7, 1, 0);
            return;
        }

        switch (dirTu2)
        {
            case Direction.Left:
                tu2.transform.rotation = Quaternion.Euler(0, 0, -90);
                dirTu2 = Direction.Right;
                break;
            case Direction.Right:
                tu2.transform.rotation = Quaternion.Euler(0, 0, 90);
                dirTu2 = Direction.Left;
                break;
        }
    }

    void Tuu3MoveSteps()
    {
        if (Vector3ToIntNode(tu3.transform.position) != 8) return;

        bool isKeyA = Input.GetKeyDown(KeyCode.A);
        bool isKeyLeft = Input.GetKeyDown(KeyCode.LeftArrow);
        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 9 && (isKeyA || isKeyLeft))
        {
            tu3.transform.position = new Vector3(-6, 0, 0);
            return;
        }

        switch (dirTu3)
        {
            case Direction.Left:
                if (Vector3ToIntNode(playerPos) == 11 && (isKeyW || isKeyUp))
                {
                    tu3.transform.position = helpers.CreateVector3(ListNodes.Lv12.node7);
                    player.transform.position = new Vector3(3, -4, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                tu3.transform.rotation = Quaternion.Euler(0, 0, -90);
                dirTu3 = Direction.Right;
                break;
            case Direction.Right:
                if (Vector3ToIntNode(playerPos) == 13 && (isKeyW || isKeyUp))
                {
                    tu3.transform.position = helpers.CreateVector3(ListNodes.Lv12.node9);
                    player.transform.position = new Vector3(3, -4, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                tu3.transform.rotation = Quaternion.Euler(0, 0, 90);
                dirTu3 = Direction.Left;
                break;
        }
    }

    void Tuu4MoveSteps()
    {
        if (Vector3ToIntNode(tu4.transform.position) != 9) return;

        bool isKeyD = Input.GetKeyDown(KeyCode.D);
        bool isKeyRight = Input.GetKeyDown(KeyCode.RightArrow);
        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 13 && (isKeyW || isKeyUp))
        {
            tu4.transform.position = new Vector3(7, 2, 0);
            return;
        }

        switch (dirTu4)
        {
            case Direction.Left:
                if (Vector3ToIntNode(playerPos) == 7 && (isKeyD || isKeyRight))
                {
                    tu4.transform.position = helpers.CreateVector3(ListNodes.Lv12.node8);
                    player.transform.position = new Vector3(3, -4, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                tu4.transform.rotation = Quaternion.Euler(0, 0, -90);
                dirTu4 = Direction.Right;
                break;
            case Direction.Right:
                tu4.transform.rotation = Quaternion.Euler(0, 0, 90);
                dirTu4 = Direction.Left;
                break;
        }
    }

    void Tuu5MoveSteps()
    {
        if (Vector3ToIntNode(tu5.transform.position) != 11) return;

        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        bool isKeyA = Input.GetKeyDown(KeyCode.A);
        bool isKeyLeft = Input.GetKeyDown(KeyCode.LeftArrow);
        Vector3 playerPos = player.transform.position;

        switch (dirTu5)
        {
            case Direction.Left:
                tu5.transform.rotation = Quaternion.Euler(0, 0, -90);
                dirTu5 = Direction.Right;
                break;
            case Direction.Right:
                if (Vector3ToIntNode(playerPos) == 17 && (isKeyW || isKeyUp))
                {
                    tu5.transform.position = helpers.CreateVector3(ListNodes.Lv12.node12);
                    player.transform.position = new Vector3(3, -4, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                if (Vector3ToIntNode(playerPos) == 12 && (isKeyA || isKeyLeft))
                {
                    tu5.transform.position = new Vector3(6, -1, 0);
                    return;
                }

                tu5.transform.rotation = Quaternion.Euler(0, 0, 90);
                dirTu5 = Direction.Left;
                break;
        }
    }

    void Tuu6MoveSteps()
    {
        if (Vector3ToIntNode(tu6.transform.position) != 15) return;

        bool isKeyA = Input.GetKeyDown(KeyCode.A);
        bool isKeyLeft = Input.GetKeyDown(KeyCode.LeftArrow);
        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        Vector3 playerPos = player.transform.position;

        switch (dirTu6)
        {
            case Direction.Left:
                if (Vector3ToIntNode(playerPos) == 18 && (isKeyW || isKeyUp))
                {
                    tu6.transform.position = helpers.CreateVector3(ListNodes.Lv12.node14);
                    player.transform.position = new Vector3(3, -4, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                tu6.transform.rotation = Quaternion.Euler(0, 0, -90);
                dirTu6 = Direction.Right;
                break;
            case Direction.Right:
                if (Vector3ToIntNode(playerPos) == 16 && (isKeyA || isKeyLeft))
                {
                    tu6.transform.position = new Vector3((float) -7.5, -3, 0);
                    return;
                }

                tu6.transform.rotation = Quaternion.Euler(0, 0, 90);
                dirTu6 = Direction.Left;
                break;
        }
    }

    // show button
    void showButton(Button button, Node node)
    {
        if (player.transform.position == helpers.CreateVector3(node))
            button.interactable = true;
        else
            button.interactable = false;
    }

    // move to button sw2
    public void moveToSw2()
    {
        player.transform.position = helpers.CreateVector3(ListNodes.Lv12.node7);
        EnemiesMoveSteps();
    }

    // move to button sw3
    public void moveToSw3()
    {
        player.transform.position = helpers.CreateVector3(ListNodes.Lv12.node10);
        EnemiesMoveSteps();
    }

    // move to button sw1
    public void moveToSw1()
    {
        player.transform.position = helpers.CreateVector3(ListNodes.Lv12.node6);
        if (dirTu2 == Direction.Right && Vector3ToIntNode(tu2.transform.position) == 5)
        {
            player.transform.position = new Vector3(3, -4, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            Time.timeScale = 0f;
        }

        EnemiesMoveSteps();
    }

    // move to button sw4
    public void moveToSw4()
    {
        player.transform.position = helpers.CreateVector3(ListNodes.Lv12.node12);
        EnemiesMoveSteps();
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 12)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap3Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap3Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap3()
    {
        Application.LoadLevel("Chapter 3");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node8)) return 8;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node9)) return 9;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node10)) return 10;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node11)) return 11;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node12)) return 12;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node13)) return 13;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node14)) return 14;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node15)) return 15;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node16)) return 16;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node17)) return 17;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node18)) return 18;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node19)) return 19;
        if (vector == helpers.CreateVector3(ListNodes.Lv12.node20)) return 20;
        return 0;
    }
}