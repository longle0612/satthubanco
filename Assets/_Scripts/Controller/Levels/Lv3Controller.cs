using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv3Controller : MonoBehaviour
{
    // Player and enemies
    private GameObject player;
    private GameObject st;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject toChap1Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Hi");
        st = GameObject.Find("Stl");
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        StMove();
        PlayerMove();
    }

    void PlayerMove()
    {
        Vector3 pos = player.transform.position;

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node2);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node4);
                        break;
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node3);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node7);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node6);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv3.node2);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv3.node3);
                        break;
                    case 8:
                        helpers.ChangePositionY(player, ListNodes.Lv3.node4);
                        break;
                    case 3:
                        helpers.ChangePositionY(player, ListNodes.Lv3.node1);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 2:
                        helpers.ChangePositionY(player, ListNodes.Lv3.node6);
                        break;
                    case 3:
                        helpers.ChangePositionY(player, ListNodes.Lv3.node7);
                        break;
                    case 4:
                        helpers.ChangePositionY(player, ListNodes.Lv3.node8);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node3);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node4);
                        break;
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node5);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node7);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv3.node8);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(pos) == 1)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 2)
            {
                level++;
                score++;
                File.WriteAllText(Data.LevelPath, level.ToString());
                File.WriteAllText(Data.ScorePath, score.ToString());
            }

            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 3 COMPLETE";
        }
    }

    void StMove()
    {
        if (player.transform.position == helpers.CreateVector3(ListNodes.Lv3.node3) &&
            st.transform.position == helpers.CreateVector3(ListNodes.Lv3.node2))
        {
            st.transform.position = helpers.CreateVector3(ListNodes.Lv3.node3);
            player.transform.position = new Vector3((float) -7.46, (float) 3.62, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
        }

        if (player.transform.position == st.transform.position)
        {
            st.transform.position = new Vector3((float) -7.46, (float) 3.62, 0);
        }
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 3)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap1Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap1Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap1()
    {
        Application.LoadLevel("Chapter 1");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv3.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv3.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv3.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv3.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv3.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv3.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv3.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv3.node8)) return 8;
        return 0;
    }
}