using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv11Controller : MonoBehaviour
{
    // Player and enemies
    private GameObject player;
    private GameObject st1;
    private GameObject st2;
    private GameObject fl1;
    private GameObject fl2;
    private GameObject fl3;

    private Button sw1Button;
    private Button sw2Button;
    private Button sw3Button;

    // Directions of enemies
    private Direction dirFl1;
    private Direction dirFl2;
    private Direction dirFl3;

    private GameObject suitCase;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject steps;
    [SerializeField] private GameObject collectBriefcase;
    [SerializeField] private GameObject toChap3Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Hi");
        fl1 = GameObject.Find("Flru 1");
        fl2 = GameObject.Find("Flru 2");
        fl3 = GameObject.Find("Flru 3");
        suitCase = GameObject.Find("SuitCase");
        dirFl1 = Direction.Right;
        dirFl2 = Direction.Right;
        dirFl3 = Direction.Right;
        st1 = GameObject.Find("Stl 1");
        st2 = GameObject.Find("Stl 2");

        sw1Button = GameObject.Find("Sw1 Button").GetComponent<Button>();
        sw2Button = GameObject.Find("Sw2 Button").GetComponent<Button>();
        sw3Button = GameObject.Find("Sw3 Button").GetComponent<Button>();

        Criterias.countSteps = 0;
        Criterias.isCollectBriefcase = false;
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        EnemiesMove();
        PlayerMove();
        switch (Data.ReadFile(Data.CriLev11Path))
        {
            case "12":
                steps.SetActive(true);
                break;
            case "13":
                collectBriefcase.SetActive(true);
                break;
            case "123":
            case "132":
                steps.SetActive(true);
                collectBriefcase.SetActive(true);
                break;
        }
    }

    void PlayerMove()
    {
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 13)
        {
            Destroy(suitCase);
            Criterias.isCollectBriefcase = true;
        }

        showButton(sw1Button, ListNodes.Lv11.node7);

        if (Vector3ToIntNode(playerPos) == 1 || Vector3ToIntNode(playerPos) == 14)
            sw2Button.interactable = true;
        else
            sw2Button.interactable = false;

        showButton(sw3Button, ListNodes.Lv11.node7);

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node3);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node4);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node5);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node6);
                        break;
                    case 9:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node8);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node11);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node9);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node10);
                        break;
                    case 14:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node13);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node16);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node14);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node15);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node5);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node4);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node6);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node9);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node12);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node11);
                        break;
                    case 9:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node10);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node16);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node17);
                        break;
                    case 14:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node15);
                        break;
                    case 13:
                        helpers.ChangePositionX(player, ListNodes.Lv11.node14);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 3:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node1);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node2);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node4);
                        break;
                    case 8:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node3);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node5);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node6);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node7);
                        break;
                    case 14:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node9);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node10);
                        break;
                    case 16:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node11);
                        break;
                    case 17:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node12);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 1:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node3);
                        break;
                    case 3:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node8);
                        break;
                    case 4:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node9);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node10);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node11);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node12);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node14);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node15);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node16);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv11.node17);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(playerPos) == 2)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 10)
            {
                File.AppendAllText(Data.CriLev11Path, "1");
                level++;
                score++;
                if (Criterias.countSteps <= 12)
                {
                    File.AppendAllText(Data.CriLev11Path, "2");
                    score++;
                }

                if (Criterias.isCollectBriefcase)
                {
                    File.AppendAllText(Data.CriLev11Path, "3");
                    score++;
                }
            }
            else
            {
                switch (File.ReadAllText(Data.CriLev11Path))
                {
                    case "1":
                        if (Criterias.countSteps <= 12)
                        {
                            File.AppendAllText(Data.CriLev11Path, "2");
                            score++;
                        }

                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev11Path, "3");
                            score++;
                        }

                        break;
                    case "12":
                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev11Path, "3");
                            score++;
                        }

                        break;
                    case "13":
                        if (Criterias.countSteps <= 12)
                        {
                            File.AppendAllText(Data.CriLev11Path, "2");
                            score++;
                        }

                        break;
                }
            }

            File.WriteAllText(Data.LevelPath, level.ToString());
            File.WriteAllText(Data.ScorePath, score.ToString());
            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 11 COMPLETE";
        }
    }

    void EnemiesMove()
    {
        if (EnemiesKillPlayer())
        {
            player.transform.position = new Vector3(-7, 3, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            Time.timeScale = 0f;
        }

        Vector3 playPos = player.transform.position;
        switch (Vector3ToIntNode(playPos))
        {
            case 1:
            case 2:
            case 7:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 3:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 4:
            case 5:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "D":
                    case "RightArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 12:
            case 6:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 8:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 9:
            case 10:
            case 11:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 13:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 14:
            case 15:
            case 16:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 17:
                switch (helpers.IsInput())
                {
                    case "W":
                    case "UpArrow":
                    case "A":
                    case "LeftArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
        }
    }

    void EnemiesMoveSteps()
    {
        Fl1MoveSteps();
        Fl2MoveSteps();
        Fl3MoveSteps();
        StMoveSteps();
        Criterias.countSteps++;
    }

    void StMoveSteps()
    {
        bool isKeyD = Input.GetKeyDown(KeyCode.D);
        bool isKeyRight = Input.GetKeyDown(KeyCode.RightArrow);
        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 3 && (isKeyD || isKeyRight))
        {
            st1.transform.position = new Vector3(-8, 0, 0);
        }

        if (Vector3ToIntNode(playerPos) == 9 && (isKeyW || isKeyUp))
        {
            st1.transform.position = new Vector3(-8, 2, 0);
        }

        if (Vector3ToIntNode(playerPos) == 10 && (isKeyW || isKeyUp))
        {
            st2.transform.position = new Vector3(-8, 2, 0);
        }
    }

    void Fl1MoveSteps()
    {
        Vector3 fl1Pos = fl1.transform.position;
        switch (dirFl1)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 6:
                        helpers.FlChangePos(fl1, ListNodes.Lv11.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl1, ListNodes.Lv11.node4);
                        break;
                    case 4:
                        helpers.FlChangePos(fl1, ListNodes.Lv11.node3);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl1 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                if (Vector3ToIntNode(fl1Pos) == 6)
                {
                    fl1.transform.rotation = Quaternion.Euler(0, 0, 90);
                    dirFl1 = Direction.Left;
                }

                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 3:
                        helpers.FlChangePos(fl1, ListNodes.Lv11.node4);
                        break;
                    case 4:
                        helpers.FlChangePos(fl1, ListNodes.Lv11.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl1, ListNodes.Lv11.node6);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl1 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl2MoveSteps()
    {
        Vector3 fl2Pos = fl2.transform.position;
        switch (dirFl2)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 10:
                        helpers.FlChangePos(fl2, ListNodes.Lv11.node9);
                        break;
                    case 12:
                        helpers.FlChangePos(fl2, ListNodes.Lv11.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl2, ListNodes.Lv11.node10);
                        break;
                    case 9:
                        helpers.FlChangePos(fl2, ListNodes.Lv11.node8);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl2 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 9:
                        helpers.FlChangePos(fl2, ListNodes.Lv11.node10);
                        break;
                    case 10:
                        helpers.FlChangePos(fl2, ListNodes.Lv11.node11);
                        break;
                    case 8:
                        helpers.FlChangePos(fl2, ListNodes.Lv11.node9);
                        break;
                    case 11:
                        helpers.FlChangePos(fl2, ListNodes.Lv11.node12);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl2 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl3MoveSteps()
    {
        Vector3 fl3Pos = fl3.transform.position;
        switch (dirFl3)
        {
            case Direction.Right:
                switch (Vector3ToIntNode(fl3Pos))
                {
                    case 14:
                        helpers.FlChangePos(fl3, ListNodes.Lv11.node15);
                        break;
                    case 13:
                        helpers.FlChangePos(fl3, ListNodes.Lv11.node14);
                        break;
                    case 15:
                        helpers.FlChangePos(fl3, ListNodes.Lv11.node16);
                        break;
                    case 16:
                        helpers.FlChangePos(fl3, ListNodes.Lv11.node17);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl3 = Direction.Left;
                        break;
                }

                break;
            case Direction.Left:
                switch (Vector3ToIntNode(fl3Pos))
                {
                    case 17:
                        helpers.FlChangePos(fl3, ListNodes.Lv11.node16);
                        break;
                    case 16:
                        helpers.FlChangePos(fl3, ListNodes.Lv11.node15);
                        break;
                    case 15:
                        helpers.FlChangePos(fl3, ListNodes.Lv11.node14);
                        break;
                    case 14:
                        helpers.FlChangePos(fl3, ListNodes.Lv11.node13);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl3 = Direction.Right;
                        break;
                }

                break;
        }
    }

    bool EnemiesKillPlayer()
    {
        if (fl1.transform.position == player.transform.position) return true;
        if (fl2.transform.position == player.transform.position) return true;
        if (fl3.transform.position == player.transform.position) return true;

        if (Vector3ToIntNode(player.transform.position) == 6 &&
            Vector3ToIntNode(st2.transform.position) == 5)
            return true;

        if (Vector3ToIntNode(player.transform.position) == 5 &&
            Vector3ToIntNode(st1.transform.position) == 4)
            return true;

        return false;
    }

    // show button
    void showButton(Button button, Node node)
    {
        if (player.transform.position == helpers.CreateVector3(node))
            button.interactable = true;
        else
            button.interactable = false;
    }

    // move to
    public void moveToSw2()
    {
        player.transform.position = helpers.CreateVector3(ListNodes.Lv11.node7);
        EnemiesMoveSteps();
    }

    public void moveToSw3()
    {
        player.transform.position = helpers.CreateVector3(ListNodes.Lv11.node14);
        EnemiesMoveSteps();
    }

    public void moveToSw1()
    {
        player.transform.position = helpers.CreateVector3(ListNodes.Lv11.node1);
        EnemiesMoveSteps();
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 11)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap3Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap3Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap3()
    {
        Application.LoadLevel("Chapter 3");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node8)) return 8;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node9)) return 9;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node10)) return 10;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node11)) return 11;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node12)) return 12;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node13)) return 13;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node14)) return 14;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node15)) return 15;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node16)) return 16;
        if (vector == helpers.CreateVector3(ListNodes.Lv11.node17)) return 17;
        return 0;
    }
}