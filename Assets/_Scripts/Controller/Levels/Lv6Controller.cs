using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv6Controller : MonoBehaviour
{
    // Player and enemies
    private GameObject player;
    private Direction dirFl1;
    private Direction dirFl2;
    private Direction dirFl3;
    private GameObject fl1;
    private GameObject fl2;
    private GameObject fl3;

    private GameObject suitCase;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject steps;
    [SerializeField] private GameObject collectBriefcase;
    [SerializeField] private GameObject toChap2Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        suitCase = GameObject.Find("SuitCase");
        dirFl1 = Direction.Left;
        dirFl2 = Direction.Left;
        dirFl3 = Direction.Right;
        fl1 = GameObject.Find("Flru1");
        fl2 = GameObject.Find("Flru2");
        fl3 = GameObject.Find("Flru3");
        player = GameObject.Find("Hi Lv6");
        Criterias.countSteps = 0;
        Criterias.isCollectBriefcase = false;
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        FlMove();
        PlayerMove();
        switch (Data.ReadFile(Data.CriLev6Path))
        {
            case "12":
                steps.SetActive(true);
                break;
            case "13":
                collectBriefcase.SetActive(true);
                break;
            case "123":
            case "132":
                steps.SetActive(true);
                collectBriefcase.SetActive(true);
                break;
        }
    }

    void PlayerMove()
    {
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 3)
        {
            Destroy(suitCase);
            Criterias.isCollectBriefcase = true;
        }

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node1);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node2);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node4);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node5);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node6);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node9);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node10);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node11);
                        break;
                    case 13:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node12);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node14);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node15);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node16);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 1:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node2);
                        break;
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node3);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node4);
                        break;
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node5);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node6);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node7);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node8);
                        break;
                    case 9:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node10);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node11);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node12);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node13);
                        break;
                    case 14:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node15);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node16);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv6.node17);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node1);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node5);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node6);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node7);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node10);
                        break;
                    case 16:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node11);
                        break;
                    case 17:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node12);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 1:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node5);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node10);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node11);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node12);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node15);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node16);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv6.node17);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(playerPos) == 8)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 5)
            {
                File.AppendAllText(Data.CriLev6Path, "1");
                level++;
                score++;
                if (Criterias.countSteps <= 12)
                {
                    File.AppendAllText(Data.CriLev6Path, "2");
                    score++;
                }

                if (Criterias.isCollectBriefcase)
                {
                    File.AppendAllText(Data.CriLev6Path, "3");
                    score++;
                }
            }
            else
            {
                switch (File.ReadAllText(Data.CriLev6Path))
                {
                    case "1":
                        if (Criterias.countSteps <= 12)
                        {
                            File.AppendAllText(Data.CriLev6Path, "2");
                            score++;
                        }

                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev6Path, "3");
                            score++;
                        }

                        break;
                    case "12":
                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev6Path, "3");
                            score++;
                        }

                        break;
                    case "13":
                        if (Criterias.countSteps <= 12)
                        {
                            File.AppendAllText(Data.CriLev6Path, "2");
                            score++;
                        }

                        break;
                }
            }

            File.WriteAllText(Data.LevelPath, level.ToString());
            File.WriteAllText(Data.ScorePath, score.ToString());
            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 6 COMPLETE";
        }
    }

    void FlMove()
    {
        if (FlKillPlayer())
        {
            player.transform.position = new Vector3(-5, 3, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            Time.timeScale = 0f;
        }

        Vector3 playerPos = player.transform.position;

        switch (Vector3ToIntNode(playerPos))
        {
            case 1:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 2:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "A":
                    case "LeftArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 3:
            case 8:
            case 13:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 4:
            case 9:
            case 14:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 5:
            case 10:
            case 11:
            case 12:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 6:
            case 7:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "S":
                    case "DownArrow":
                    case "A":
                    case "LeftArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 15:
            case 16:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
            case 17:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                        FlMoveSteps();
                        break;
                }

                break;
        }
    }

    void FlMoveSteps()
    {
        Fl1MoveSteps();
        Fl2MoveSteps();
        Fl3MoveSteps();
        Criterias.countSteps++;
    }

    void Fl1MoveSteps()
    {
        Vector3 fl1Pos = fl1.transform.position;
        switch (dirFl1)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 8:
                        helpers.FlChangePos(fl1, ListNodes.Lv6.node7);
                        break;
                    case 7:
                        helpers.FlChangePos(fl1, ListNodes.Lv6.node6);
                        break;
                    case 6:
                        helpers.FlChangePos(fl1, ListNodes.Lv6.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl1, ListNodes.Lv6.node4);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl1 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 4:
                        helpers.FlChangePos(fl1, ListNodes.Lv6.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl1, ListNodes.Lv6.node6);
                        break;
                    case 6:
                        helpers.FlChangePos(fl1, ListNodes.Lv6.node7);
                        break;
                    case 7:
                        helpers.FlChangePos(fl1, ListNodes.Lv6.node8);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl1 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl2MoveSteps()
    {
        Vector3 fl2Pos = fl2.transform.position;
        switch (dirFl2)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 13:
                        helpers.FlChangePos(fl2, ListNodes.Lv6.node12);
                        break;
                    case 12:
                        helpers.FlChangePos(fl2, ListNodes.Lv6.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl2, ListNodes.Lv6.node10);
                        break;
                    case 10:
                        helpers.FlChangePos(fl2, ListNodes.Lv6.node9);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl2 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 9:
                        helpers.FlChangePos(fl2, ListNodes.Lv6.node10);
                        break;
                    case 10:
                        helpers.FlChangePos(fl2, ListNodes.Lv6.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl2, ListNodes.Lv6.node12);
                        break;
                    case 12:
                        helpers.FlChangePos(fl2, ListNodes.Lv6.node13);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl2 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl3MoveSteps()
    {
        Vector3 fl3Pos = fl3.transform.position;
        switch (dirFl3)
        {
            case Direction.Right:
                switch (Vector3ToIntNode(fl3Pos))
                {
                    case 9:
                        helpers.FlChangePos(fl3, ListNodes.Lv6.node10);
                        break;
                    case 10:
                        helpers.FlChangePos(fl3, ListNodes.Lv6.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl3, ListNodes.Lv6.node12);
                        break;
                    case 12:
                        helpers.FlChangePos(fl3, ListNodes.Lv6.node13);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl3 = Direction.Left;
                        break;
                }

                break;
            case Direction.Left:
                switch (Vector3ToIntNode(fl3Pos))
                {
                    case 13:
                        helpers.FlChangePos(fl3, ListNodes.Lv6.node12);
                        break;
                    case 12:
                        helpers.FlChangePos(fl3, ListNodes.Lv6.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl3, ListNodes.Lv6.node10);
                        break;
                    case 10:
                        helpers.FlChangePos(fl3, ListNodes.Lv6.node9);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl3 = Direction.Right;
                        break;
                }

                break;
        }
    }

    bool FlKillPlayer()
    {
        if (fl1.transform.position == player.transform.position) return true;

        if (fl2.transform.position == player.transform.position &&
            player.transform.position != helpers.CreateVector3(ListNodes.Lv6.node11)) return true;

        if (fl3.transform.position == player.transform.position &&
            player.transform.position != helpers.CreateVector3(ListNodes.Lv6.node11)) return true;

        return false;
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 6)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap2Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap2Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap2()
    {
        Application.LoadLevel("Chapter 2");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node8)) return 8;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node9)) return 9;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node10)) return 10;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node11)) return 11;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node12)) return 12;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node13)) return 13;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node14)) return 14;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node15)) return 15;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node16)) return 16;
        if (vector == helpers.CreateVector3(ListNodes.Lv6.node17)) return 17;
        return 0;
    }
}