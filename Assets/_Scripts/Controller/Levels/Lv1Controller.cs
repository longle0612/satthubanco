using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv1Controller : MonoBehaviour
{
    private GameObject player;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject toChap1Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Hi");
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        PlayerMove();
    }

    // Rules of player movement
    void PlayerMove()
    {
        Vector3 pos = player.transform.position;

        switch (helpers.IsInput())
        {
            case "D":
            case "RightArrow":
                if (Vector3ToIntNode(pos) == 1) helpers.ChangePositionX(player, ListNodes.Lv1.node2);
                else if (Vector3ToIntNode(pos) == 2) helpers.ChangePositionX(player, ListNodes.Lv1.node3);
                break;
            case "A":
            case "LeftArrow":
                if (Vector3ToIntNode(pos) == 2) helpers.ChangePositionX(player, ListNodes.Lv1.node1);
                break;
        }

        if (Vector3ToIntNode(pos) == 3)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 0)
            {
                level++;
                score++;
                File.WriteAllText(Data.LevelPath, level.ToString());
                File.WriteAllText(Data.ScorePath, score.ToString());
            }

            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 1 COMPLETE";
        }
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 1)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap1Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap1Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap1()
    {
        Application.LoadLevel("Chapter 1");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv1.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv1.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv1.node3)) return 3;
        return 0;
    }
}