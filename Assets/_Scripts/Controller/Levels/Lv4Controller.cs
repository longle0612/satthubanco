using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv4Controller : MonoBehaviour
{
    // Player and enemies
    private GameObject player;
    private GameObject st1;
    private GameObject st2;
    private GameObject st3;
    private GameObject st4;
    private GameObject st5;
    private GameObject st6;
    private GameObject st7;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject toChap1Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Hi");
        st1 = GameObject.Find("Stl 1");
        st2 = GameObject.Find("Stl 2");
        st3 = GameObject.Find("Stl 3");
        st4 = GameObject.Find("Stl 4");
        st5 = GameObject.Find("Stl 5");
        st6 = GameObject.Find("Stl 6");
        st7 = GameObject.Find("Stl 7");
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        StMove();
        PlayerMove();
    }

    void PlayerMove()
    {
        if (EnemiesKillPlayer())
        {
            player.transform.position = new Vector3(7, -4, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
        }

        Vector3 playerPos = player.transform.position;

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node1);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node2);
                        break;
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node3);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node5);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node6);
                        break;
                    case 9:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node8);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node10);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node11);
                        break;
                    case 13:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node12);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node15);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node16);
                        break;
                    case 18:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node17);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node10);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node5);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node1);
                        break;
                    case 16:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node11);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node6);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node2);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node7);
                        break;
                    case 18:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node13);
                        break;
                    case 13:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node8);
                        break;
                    case 8:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node4);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 1:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node5);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node10);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node15);
                        break;
                    case 2:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node6);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node11);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node16);
                        break;
                    case 13:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node18);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node12);
                        break;
                    case 4:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node8);
                        break;
                    case 8:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node13);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv4.node14);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(playerPos))
                {
                    case 1:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node2);
                        break;
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node3);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node4);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node6);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node7);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node9);
                        break;
                    case 10:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node11);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node12);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node13);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node18);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node16);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv4.node17);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(playerPos) == 14)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 3)
            {
                level++;
                score++;
                File.WriteAllText(Data.LevelPath, level.ToString());
                File.WriteAllText(Data.ScorePath, score.ToString());
            }

            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 4 COMPLETE";
        }
    }

    void StMove()
    {
        if (player.transform.position == st1.transform.position)
        {
            st1.transform.position = new Vector3((float) 2.5, (float) 2.5, 0);
        }

        if (player.transform.position == st2.transform.position)
        {
            st2.transform.position = new Vector3(-8, 4, 0);
        }

        if (player.transform.position == st3.transform.position)
        {
            st3.transform.position = new Vector3(0, 3, 0);
        }

        if (player.transform.position == st4.transform.position)
        {
            st4.transform.position = new Vector3(-8, 2, 0);
        }

        if (player.transform.position == st5.transform.position)
        {
            st5.transform.position = new Vector3(-1, -3, 0);
        }

        if (player.transform.position == st6.transform.position)
        {
            st6.transform.position = new Vector3(6, -3, 0);
        }

        if (player.transform.position == st7.transform.position)
        {
            st7.transform.position = new Vector3(-8, 0, 0);
        }
    }

    bool EnemiesKillPlayer()
    {
        if (Vector3ToIntNode(st1.transform.position) == 4 && Vector3ToIntNode(player.transform.position) == 8)
        {
            st1.transform.position = helpers.CreateVector3(ListNodes.Lv4.node8);
            return true;
        }

        if (Vector3ToIntNode(st2.transform.position) == 6 && Vector3ToIntNode(player.transform.position) == 2)
        {
            st2.transform.position = helpers.CreateVector3(ListNodes.Lv4.node2);
            return true;
        }

        if (Vector3ToIntNode(st3.transform.position) == 7 && Vector3ToIntNode(player.transform.position) == 6)
        {
            st3.transform.position = helpers.CreateVector3(ListNodes.Lv4.node6);
            return true;
        }

        if (Vector3ToIntNode(st4.transform.position) == 11 && Vector3ToIntNode(player.transform.position) == 6)
        {
            st4.transform.position = helpers.CreateVector3(ListNodes.Lv4.node6);
            return true;
        }

        if (Vector3ToIntNode(st5.transform.position) == 12 && Vector3ToIntNode(player.transform.position) == 11)
        {
            st5.transform.position = helpers.CreateVector3(ListNodes.Lv4.node11);
            return true;
        }

        if (Vector3ToIntNode(st6.transform.position) == 13 && Vector3ToIntNode(player.transform.position) == 8)
        {
            st6.transform.position = helpers.CreateVector3(ListNodes.Lv4.node8);
            return true;
        }

        if (Vector3ToIntNode(st7.transform.position) == 15 && Vector3ToIntNode(player.transform.position) == 16)
        {
            st7.transform.position = helpers.CreateVector3(ListNodes.Lv4.node16);
            return true;
        }

        return false;
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 4)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap1Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap1Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap1()
    {
        Application.LoadLevel("Chapter 1");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node8)) return 8;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node9)) return 9;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node10)) return 10;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node11)) return 11;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node12)) return 12;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node13)) return 13;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node14)) return 14;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node15)) return 15;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node16)) return 16;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node17)) return 17;
        if (vector == helpers.CreateVector3(ListNodes.Lv4.node18)) return 18;
        return 0;
    }
}