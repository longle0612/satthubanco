using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lv9Controller : MonoBehaviour
{
    // Player and enemies
    private GameObject player;
    private GameObject fl1;
    private GameObject fl2;
    private GameObject fl3;
    private GameObject tu1;
    private GameObject tu2;
    private GameObject tu3;

    // Directions of enemies
    private Direction dirFl1;
    private Direction dirFl2;
    private Direction dirFl3;
    private Direction dirTu1;
    private Direction dirTu2;
    private Direction dirTu3;

    // items
    private GameObject suitCase;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject losePanel;
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private Text title;
    [SerializeField] private GameObject complete;
    [SerializeField] private GameObject steps;
    [SerializeField] private GameObject collectBriefcase;
    [SerializeField] private GameObject toChap3Button;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject xButton;

    // Start is called before the first frame update

    void Start()
    {
        suitCase = GameObject.Find("SuitCase");
        dirFl1 = Direction.Left;
        dirFl2 = Direction.Left;
        dirFl3 = Direction.Up;
        fl1 = GameObject.Find("Flru1");
        fl2 = GameObject.Find("Flru2");
        fl3 = GameObject.Find("Flru3");
        dirTu1 = Direction.Up;
        dirTu2 = Direction.Up;
        dirTu3 = Direction.Down;
        tu1 = GameObject.Find("Tuu1");
        tu2 = GameObject.Find("Tuu2");
        tu3 = GameObject.Find("Tuu3");
        player = GameObject.Find("Hi");
        Criterias.countSteps = 0;
        Criterias.isCollectBriefcase = false;
    }

    // Update is called once per frame

    void Update()
    {
        helpers.CheckMute(audioSource);
        EnemiesMove();
        PlayerMove();
        switch (Data.ReadFile(Data.CriLev9Path))
        {
            case "12":
                steps.SetActive(true);
                break;
            case "13":
                collectBriefcase.SetActive(true);
                break;
            case "123":
            case "132":
                steps.SetActive(true);
                collectBriefcase.SetActive(true);
                break;
        }
    }

    void PlayerMove()
    {
        Vector3 pos = player.transform.position;

        if (Vector3ToIntNode(pos) == 2)
        {
            Destroy(suitCase);
            Criterias.isCollectBriefcase = true;
        }

        switch (helpers.IsInput())
        {
            case "A":
            case "LeftArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node1);
                        break;
                    case 3:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node2);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node4);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node5);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node6);
                        break;
                    case 8:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node7);
                        break;
                    case 12:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node11);
                        break;
                    case 14:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node13);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node14);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node15);
                        break;
                    case 17:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node16);
                        break;
                    case 19:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node18);
                        break;
                    case 20:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node19);
                        break;
                    case 21:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node20);
                        break;
                }

                break;
            case "D":
            case "RightArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 1:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node2);
                        break;
                    case 2:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node3);
                        break;
                    case 4:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node5);
                        break;
                    case 5:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node6);
                        break;
                    case 6:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node7);
                        break;
                    case 7:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node8);
                        break;
                    case 11:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node12);
                        break;
                    case 13:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node14);
                        break;
                    case 14:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node15);
                        break;
                    case 15:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node16);
                        break;
                    case 16:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node17);
                        break;
                    case 18:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node19);
                        break;
                    case 19:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node20);
                        break;
                    case 20:
                        helpers.ChangePositionX(player, ListNodes.Lv9.node21);
                        break;
                }

                break;
            case "W":
            case "UpArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node3);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node5);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node6);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node7);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node8);
                        break;
                    case 14:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node9);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node10);
                        break;
                    case 16:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node11);
                        break;
                    case 17:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node12);
                        break;
                    case 20:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node15);
                        break;
                    case 21:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node16);
                        break;
                }

                break;
            case "S":
            case "DownArrow":
                switch (Vector3ToIntNode(pos))
                {
                    case 3:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node6);
                        break;
                    case 5:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node9);
                        break;
                    case 6:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node10);
                        break;
                    case 7:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node11);
                        break;
                    case 8:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node12);
                        break;
                    case 9:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node14);
                        break;
                    case 10:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node15);
                        break;
                    case 11:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node16);
                        break;
                    case 12:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node17);
                        break;
                    case 15:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node20);
                        break;
                    case 16:
                        helpers.ChangePositionY(player, ListNodes.Lv9.node21);
                        break;
                }

                break;
        }

        if (Vector3ToIntNode(pos) == 13)
        {
            int level = Convert.ToInt32(File.ReadAllText(Data.LevelPath));
            int score = Convert.ToInt32(File.ReadAllText(Data.ScorePath));
            if (level == 8)
            {
                File.AppendAllText(Data.CriLev9Path, "1");
                level++;
                score++;
                if (Criterias.countSteps <= 25)
                {
                    File.AppendAllText(Data.CriLev9Path, "2");
                    score++;
                }

                if (Criterias.isCollectBriefcase)
                {
                    File.AppendAllText(Data.CriLev9Path, "3");
                    score++;
                }
            }
            else
            {
                switch (File.ReadAllText(Data.CriLev9Path))
                {
                    case "1":
                        if (Criterias.countSteps <= 25)
                        {
                            File.AppendAllText(Data.CriLev9Path, "2");
                            score++;
                        }

                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev9Path, "3");
                            score++;
                        }

                        break;
                    case "12":
                        if (Criterias.isCollectBriefcase)
                        {
                            File.AppendAllText(Data.CriLev9Path, "3");
                            score++;
                        }

                        break;
                    case "13":
                        if (Criterias.countSteps <= 25)
                        {
                            File.AppendAllText(Data.CriLev9Path, "2");
                            score++;
                        }

                        break;
                }
            }

            File.WriteAllText(Data.LevelPath, level.ToString());
            File.WriteAllText(Data.ScorePath, score.ToString());
            statusPanel.SetActive(true);
            complete.SetActive(true);
            nextButton.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            title.text = "LEVEL 9 COMPLETE";
        }
    }

    void EnemiesMove()
    {
        if (FlKillPlayer())
        {
            player.transform.position = new Vector3(-7, 3, 0);
            losePanel.SetActive(true);
            replayButton.SetActive(false);
            pauseButton.SetActive(false);
            Time.timeScale = 0f;
        }

        Vector3 playerPos = player.transform.position;

        switch (Vector3ToIntNode(playerPos))
        {
            case 1:
            case 4:
            case 13:
            case 18:
                switch (helpers.IsInput())
                {
                    case "D":
                    case "RightArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 2:
            case 19:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 3:
            case 8:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 5:
            case 7:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                    case "D":
                    case "RightArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 6:
            case 15:
            case 16:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 9:
            case 10:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 11:
                switch (helpers.IsInput())
                {
                    case "S":
                    case "DownArrow":
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 12:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "S":
                    case "DownArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 14:
            case 20:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "D":
                    case "RightArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
            case 17:
            case 21:
                switch (helpers.IsInput())
                {
                    case "A":
                    case "LeftArrow":
                    case "W":
                    case "UpArrow":
                        EnemiesMoveSteps();
                        break;
                }

                break;
        }
    }

    void EnemiesMoveSteps()
    {
        Tuu1MoveSteps();
        Tuu2MoveSteps();
        Tuu3MoveSteps();
        Fl1MoveSteps();
        Fl2MoveSteps();
        Fl3MoveSteps();
        Criterias.countSteps++;
    }

    void Tuu1MoveSteps()
    {
        if (Vector3ToIntNode(tu1.transform.position) != 6) return;

        bool isKeyA = Input.GetKeyDown(KeyCode.A);
        bool isKeyLeft = Input.GetKeyDown(KeyCode.LeftArrow);
        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 7 && (isKeyA || isKeyLeft))
            tu1.transform.position = new Vector3(-7, 4, 0);

        switch (dirTu1)
        {
            case Direction.Up:
                tu1.transform.rotation = Quaternion.Euler(0, 0, 180);
                dirTu1 = Direction.Down;
                break;
            case Direction.Down:
                if (Vector3ToIntNode(playerPos) == 15 && (isKeyW || isKeyUp))
                {
                    tu1.transform.position = helpers.CreateVector3(ListNodes.Lv9.node10);
                    player.transform.position = new Vector3(-7, 3, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                tu1.transform.rotation = Quaternion.Euler(0, 0, 360);
                dirTu1 = Direction.Up;
                break;
        }
    }

    void Tuu2MoveSteps()
    {
        if (Vector3ToIntNode(tu2.transform.position) != 9) return;

        bool isKeyA = Input.GetKeyDown(KeyCode.A);
        bool isKeyLeft = Input.GetKeyDown(KeyCode.LeftArrow);
        bool isKeyS = Input.GetKeyDown(KeyCode.S);
        bool isKeyDown = Input.GetKeyDown(KeyCode.DownArrow);
        Vector3 playerPos = player.transform.position;

        if (Vector3ToIntNode(playerPos) == 5 && (isKeyS || isKeyDown))
            tu2.transform.position = new Vector3(-7, 0, 0);

        switch (dirTu2)
        {
            case Direction.Up:
                tu2.transform.rotation = Quaternion.Euler(0, 0, 180);
                dirTu2 = Direction.Down;
                break;
            case Direction.Down:
                if (Vector3ToIntNode(playerPos) == 15 && (isKeyA || isKeyLeft))
                {
                    tu2.transform.position = helpers.CreateVector3(ListNodes.Lv9.node14);
                    player.transform.position = new Vector3(-7, 3, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                tu2.transform.rotation = Quaternion.Euler(0, 0, 360);
                dirTu2 = Direction.Up;
                break;
        }
    }

    void Tuu3MoveSteps()
    {
        if (Vector3ToIntNode(tu3.transform.position) != 11) return;

        bool isKeyA = Input.GetKeyDown(KeyCode.A);
        bool isKeyLeft = Input.GetKeyDown(KeyCode.LeftArrow);
        bool isKeyW = Input.GetKeyDown(KeyCode.W);
        bool isKeyUp = Input.GetKeyDown(KeyCode.UpArrow);

        Vector3 playerPos = player.transform.position;

        switch (Vector3ToIntNode(playerPos))
        {
            case 12:
                if (isKeyA || isKeyLeft) tu3.transform.position = new Vector3(-7, 2, 0);
                break;
            case 16:
                if (isKeyW || isKeyUp) tu3.transform.position = new Vector3(-7, 2, 0);
                break;
        }

        switch (dirTu3)
        {
            case Direction.Up:
                if (Vector3ToIntNode(playerPos) == 8 && (isKeyA || isKeyLeft))
                {
                    tu2.transform.position = helpers.CreateVector3(ListNodes.Lv9.node10);
                    player.transform.position = new Vector3(-7, 3, 0);
                    losePanel.SetActive(true);
                    replayButton.SetActive(false);
                    pauseButton.SetActive(false);
                    Time.timeScale = 0f;
                }

                tu3.transform.rotation = Quaternion.Euler(0, 0, 180);
                dirTu3 = Direction.Down;
                break;
            case Direction.Down:
                tu3.transform.rotation = Quaternion.Euler(0, 0, 360);
                dirTu3 = Direction.Up;
                break;
        }
    }

    void Fl1MoveSteps()
    {
        Vector3 fl1Pos = fl1.transform.position;
        switch (dirFl1)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 8:
                        helpers.FlChangePos(fl1, ListNodes.Lv9.node7);
                        break;
                    case 7:
                        helpers.FlChangePos(fl1, ListNodes.Lv9.node6);
                        break;
                    case 6:
                        helpers.FlChangePos(fl1, ListNodes.Lv9.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl1, ListNodes.Lv9.node4);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl1 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl1Pos))
                {
                    case 4:
                        helpers.FlChangePos(fl1, ListNodes.Lv9.node5);
                        break;
                    case 5:
                        helpers.FlChangePos(fl1, ListNodes.Lv9.node6);
                        break;
                    case 6:
                        helpers.FlChangePos(fl1, ListNodes.Lv9.node7);
                        break;
                    case 7:
                        helpers.FlChangePos(fl1, ListNodes.Lv9.node8);
                        fl1.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl1 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl2MoveSteps()
    {
        Vector3 fl2Pos = fl2.transform.position;
        switch (dirFl2)
        {
            case Direction.Left:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 17:
                        helpers.FlChangePos(fl2, ListNodes.Lv9.node16);
                        break;
                    case 16:
                        helpers.FlChangePos(fl2, ListNodes.Lv9.node15);
                        break;
                    case 15:
                        helpers.FlChangePos(fl2, ListNodes.Lv9.node14);
                        break;
                    case 14:
                        helpers.FlChangePos(fl2, ListNodes.Lv9.node13);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, -90);
                        dirFl2 = Direction.Right;
                        break;
                }

                break;
            case Direction.Right:
                switch (Vector3ToIntNode(fl2Pos))
                {
                    case 13:
                        helpers.FlChangePos(fl2, ListNodes.Lv9.node14);
                        break;
                    case 14:
                        helpers.FlChangePos(fl2, ListNodes.Lv9.node15);
                        break;
                    case 15:
                        helpers.FlChangePos(fl2, ListNodes.Lv9.node16);
                        break;
                    case 16:
                        helpers.FlChangePos(fl2, ListNodes.Lv9.node17);
                        fl2.transform.rotation = Quaternion.Euler(0, 0, 90);
                        dirFl2 = Direction.Left;
                        break;
                }

                break;
        }
    }

    void Fl3MoveSteps()
    {
        Vector3 fl3Pos = fl3.transform.position;
        switch (dirFl3)
        {
            case Direction.Up:
                switch (Vector3ToIntNode(fl3Pos))
                {
                    case 21:
                        helpers.FlChangePos(fl3, ListNodes.Lv9.node16);
                        break;
                    case 16:
                        helpers.FlChangePos(fl3, ListNodes.Lv9.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl3, ListNodes.Lv9.node7);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, 180);
                        dirFl3 = Direction.Down;
                        break;
                }

                break;
            case Direction.Down:
                switch (Vector3ToIntNode(fl3Pos))
                {
                    case 7:
                        helpers.FlChangePos(fl3, ListNodes.Lv9.node11);
                        break;
                    case 11:
                        helpers.FlChangePos(fl3, ListNodes.Lv9.node16);
                        break;
                    case 16:
                        helpers.FlChangePos(fl3, ListNodes.Lv9.node21);
                        fl3.transform.rotation = Quaternion.Euler(0, 0, 360);
                        dirFl3 = Direction.Up;
                        break;
                }

                break;
        }
    }

    bool FlKillPlayer()
    {
        if (fl1.transform.position == player.transform.position) return true;
        if (fl2.transform.position == player.transform.position) return true;
        if (fl3.transform.position == player.transform.position) return true;
        return false;
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        if (Convert.ToInt32(File.ReadAllText(Data.LevelPath)) >= 9)
            complete.SetActive(true);
        replayButton.SetActive(false);
        pauseButton.SetActive(false);
        statusPanel.SetActive(true);
        toChap3Button.SetActive(true);
        xButton.SetActive(true);
    }

    public void XButton()
    {
        toChap3Button.SetActive(false);
        xButton.SetActive(false);
        statusPanel.SetActive(false);
        replayButton.SetActive(true);
        pauseButton.SetActive(true);
    }

    public void ToChap3()
    {
        Application.LoadLevel("Chapter 3");
    }

    // The return value indicates which node the object is at
    int Vector3ToIntNode(Vector3 vector)
    {
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node1)) return 1;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node2)) return 2;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node3)) return 3;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node4)) return 4;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node5)) return 5;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node6)) return 6;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node7)) return 7;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node8)) return 8;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node9)) return 9;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node10)) return 10;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node11)) return 11;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node12)) return 12;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node13)) return 13;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node14)) return 14;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node15)) return 15;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node16)) return 16;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node17)) return 17;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node18)) return 18;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node19)) return 19;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node20)) return 20;
        if (vector == helpers.CreateVector3(ListNodes.Lv9.node21)) return 21;
        return 0;
    }
}