using System.IO;
using UnityEngine;

public class SettingController : MonoBehaviour
{
    [SerializeField] private GameObject SettingPanel;
    [SerializeField] private GameObject ResetPanel;
    [SerializeField] private GameObject AudioPanel;
    [SerializeField] private GameObject ResetSuccessPanel;

    public void ResetButton()
    {
        ResetPanel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void AudioButton()
    {
        AudioPanel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void BackToMainButton()
    {
        SettingPanel.SetActive(false);
        Time.timeScale = 1f;
    }

    public void YesButton()
    {
        Data.DeleteFile(Data.LevelPath);
        Data.DeleteFile(Data.ScorePath);
        File.WriteAllText(Data.MutePath, "false");
        Data.DeleteFile(Data.CriLev5Path);
        Data.DeleteFile(Data.CriLev6Path);
        Data.DeleteFile(Data.CriLev7Path);
        Data.DeleteFile(Data.CriLev8Path);
        Data.DeleteFile(Data.CriLev9Path);
        Data.DeleteFile(Data.CriLev10Path);
        Data.DeleteFile(Data.CriLev11Path);
        Data.DeleteFile(Data.CriLev12Path);
        ResetPanel.SetActive(false);
        SettingPanel.SetActive(false);
        ResetSuccessPanel.SetActive(true);
    }

    public void NoButton()
    {
        ResetPanel.SetActive(false);
    }

    public void OkButton()
    {
        ResetSuccessPanel.SetActive(false);
        SettingPanel.SetActive(true);
    }

    public void MuteButton()
    {
        File.WriteAllText(Data.MutePath, "true");
        AudioPanel.SetActive(false);
    }

    public void UnmuteButton()
    {
        File.WriteAllText(Data.MutePath, "false");
        AudioPanel.SetActive(false);
    }
}