using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Chapter1 : MonoBehaviour
{
    // Level Buttons
    private Button Lv1Button;
    private Button Lv2Button;
    private Button Lv3Button;
    private Button Lv4Button;

    // Level complete buttons
    private Button Lv1CplBut;
    private Button Lv2CplBut;
    private Button Lv3CplBut;
    private Button Lv4CplBut;

    private int checkLv;
    [SerializeField] private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        Lv1Button = helpers.FindButton("Lv1 Button");
        Lv2Button = helpers.FindButton("Lv2 Button");
        Lv3Button = helpers.FindButton("Lv3 Button");
        Lv4Button = helpers.FindButton("Lv4 Button");
        Lv1CplBut = helpers.FindButton("Lv1 Complete Button");
        Lv2CplBut = helpers.FindButton("Lv2 Complete Button");
        Lv3CplBut = helpers.FindButton("Lv3 Complete Button");
        Lv4CplBut = helpers.FindButton("Lv4 Complete Button");
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        checkLv = Convert.ToInt32(File.ReadAllText(Data.LevelPath));

        if (checkLv >= 1)
        {
            Lv1CplBut.interactable = true;
            Lv2Button.interactable = true;
        }

        if (checkLv >= 2)
        {
            Lv2CplBut.interactable = true;
            Lv3Button.interactable = true;
        }

        if (checkLv >= 3)
        {
            Lv3CplBut.interactable = true;
            Lv4Button.interactable = true;
        }

        if (checkLv >= 4) Lv4CplBut.interactable = true;
    }

    public void HomeButton()
    {
        Application.LoadLevel("MainMenu");
    }

    public void Back()
    {
        Application.LoadLevel("Choose Chapter");
    }

    public void Level1Button()
    {
        Application.LoadLevel("Level 1");
    }

    public void Level2Button()
    {
        Application.LoadLevel("Level 2");
    }

    public void Level3Button()
    {
        Application.LoadLevel("Level 3");
    }

    public void Level4Button()
    {
        Application.LoadLevel("Level 4");
    }
}