using System;
using UnityEngine;
using UnityEngine.UI;

public class Chapter2 : MonoBehaviour
{
    // Level Buttons
    private Button Lv5Button;
    private Button Lv6Button;
    private Button Lv7Button;
    private Button Lv8Button;

    // Criteria Buttons
    // Level complete buttons
    private Button Lv5CplBut;
    private Button Lv6CplBut;
    private Button Lv7CplBut;
    private Button Lv8CplBut;

    // Step buttons
    private Button Lv5SteBut;
    private Button Lv6SteBut;
    private Button Lv7SteBut;

    // No kill button
    private Button Lv8NoKillBut;

    // Collect briefcase buttons
    private Button Lv5ColBriBut;
    private Button Lv6ColBriBut;
    private Button Lv7ColBriBut;
    private Button Lv8ColBriBut;

    private int checkLv;
    [SerializeField] private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        Lv5Button = helpers.FindButton("Lv5 Button");
        Lv6Button = helpers.FindButton("Lv6 Button");
        Lv7Button = helpers.FindButton("Lv7 Button");
        Lv8Button = helpers.FindButton("Lv8 Button");
        Lv5CplBut = helpers.FindButton("Lv5 Complete Button");
        Lv6CplBut = helpers.FindButton("Lv6 Complete Button");
        Lv7CplBut = helpers.FindButton("Lv7 Complete Button");
        Lv8CplBut = helpers.FindButton("Lv8 Complete Button");
        Lv5SteBut = helpers.FindButton("Lv5 Steps Button");
        Lv6SteBut = helpers.FindButton("Lv6 Steps Button");
        Lv7SteBut = helpers.FindButton("Lv7 Steps Button");
        Lv8NoKillBut = helpers.FindButton("Lv8 NoKill Button");
        Lv5ColBriBut = helpers.FindButton("Lv5 Briefcase Button");
        Lv6ColBriBut = helpers.FindButton("Lv6 Briefcase Button");
        Lv7ColBriBut = helpers.FindButton("Lv7 Briefcase Button");
        Lv8ColBriBut = helpers.FindButton("Lv8 Briefcase Button");
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        checkLv = Convert.ToInt32(Data.ReadFile(Data.LevelPath));

        if (checkLv >= 4)
        {
            Lv5Button.interactable = true;
            switch (Data.ReadFile(Data.CriLev5Path))
            {
                case "12":
                    Lv5SteBut.interactable = true;
                    break;
                case "13":
                    Lv5ColBriBut.interactable = true;
                    break;
                case "123":
                case "132":
                    Lv5SteBut.interactable = true;
                    Lv5ColBriBut.interactable = true;
                    break;
            }
        }

        if (checkLv >= 5)
        {
            Lv5CplBut.interactable = true;
            Lv6Button.interactable = true;
            switch (Data.ReadFile(Data.CriLev6Path))
            {
                case "12":
                    Lv6SteBut.interactable = true;
                    break;
                case "13":
                    Lv6ColBriBut.interactable = true;
                    break;
                case "123":
                case "132":
                    Lv6SteBut.interactable = true;
                    Lv6ColBriBut.interactable = true;
                    break;
            }
        }

        if (checkLv >= 6)
        {
            Lv6CplBut.interactable = true;
            Lv7Button.interactable = true;
            switch (Data.ReadFile(Data.CriLev7Path))
            {
                case "12":
                    Lv7SteBut.interactable = true;
                    break;
                case "13":
                    Lv7ColBriBut.interactable = true;
                    break;
                case "123":
                case "132":
                    Lv7SteBut.interactable = true;
                    Lv7ColBriBut.interactable = true;
                    break;
            }
        }

        if (checkLv >= 7)
        {
            Lv7CplBut.interactable = true;
            Lv8Button.interactable = true;
            switch (Data.ReadFile(Data.CriLev8Path))
            {
                case "12":
                    Lv8NoKillBut.interactable = true;
                    break;
                case "13":
                    Lv8ColBriBut.interactable = true;
                    break;
                case "123":
                case "132":
                    Lv8NoKillBut.interactable = true;
                    Lv8ColBriBut.interactable = true;
                    break;
            }
        }

        if (checkLv >= 8) Lv8CplBut.interactable = true;
    }

    public void HomeButton()
    {
        Application.LoadLevel("MainMenu");
    }

    public void Back()
    {
        Application.LoadLevel("Choose Chapter");
    }

    public void Level5Button()
    {
        Application.LoadLevel("Level 5");
    }

    public void Level6Button()
    {
        Application.LoadLevel("Level 6");
    }

    public void Level7Button()
    {
        Application.LoadLevel("Level 7");
    }

    public void Level8Button()
    {
        Application.LoadLevel("Level 8");
    }
}