using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Chapter3 : MonoBehaviour
{
    private Button Lv9Button;
    private Button Lv10Button;
    private Button Lv11Button;
    private Button Lv12Button;
    private int checkLv;

    // Criteria Buttons
    // Level complete buttons
    private Button Lv9CplBut;
    private Button Lv10CplBut;
    private Button Lv11CplBut;
    private Button Lv12CplBut;

    // Step buttons
    private Button Lv9SteBut;
    private Button Lv10SteBut;
    private Button Lv11SteBut;
    private Button Lv12SteBut;

    // Collect briefcase buttons
    private Button Lv9ColBriBut;
    private Button Lv10ColBriBut;
    private Button Lv11ColBriBut;
    private Button Lv12ColBriBut;

    [SerializeField] private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        Lv9Button = GameObject.Find("Lv9 Button").GetComponent<Button>();
        Lv10Button = GameObject.Find("Lv10 Button").GetComponent<Button>();
        Lv11Button = GameObject.Find("Lv11 Button").GetComponent<Button>();
        Lv12Button = GameObject.Find("Lv12 Button").GetComponent<Button>();
        Lv9CplBut = helpers.FindButton("Lv9 Complete Button");
        Lv10CplBut = helpers.FindButton("Lv10 Complete Button");
        Lv11CplBut = helpers.FindButton("Lv11 Complete Button");
        Lv12CplBut = helpers.FindButton("Lv12 Complete Button");
        Lv9SteBut = helpers.FindButton("Lv9 Steps Button");
        Lv10SteBut = helpers.FindButton("Lv10 Steps Button");
        Lv11SteBut = helpers.FindButton("Lv11 Steps Button");
        Lv12SteBut = helpers.FindButton("Lv12 Steps Button");
        Lv9ColBriBut = helpers.FindButton("Lv9 Briefcase Button");
        Lv10ColBriBut = helpers.FindButton("Lv10 Briefcase Button");
        Lv11ColBriBut = helpers.FindButton("Lv11 Briefcase Button");
        Lv12ColBriBut = helpers.FindButton("Lv12 Briefcase Button");
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        checkLv = Convert.ToInt32(File.ReadAllText(Data.LevelPath));

        if (checkLv >= 8)
        {
            Lv9Button.interactable = true;
            switch (Data.ReadFile(Data.CriLev9Path))
            {
                case "12":
                    Lv9SteBut.interactable = true;
                    break;
                case "13":
                    Lv9ColBriBut.interactable = true;
                    break;
                case "123":
                case "132":
                    Lv9SteBut.interactable = true;
                    Lv9ColBriBut.interactable = true;
                    break;
            }
        }

        if (checkLv >= 9)
        {
            Lv9CplBut.interactable = true;
            Lv10Button.interactable = true;
            switch (Data.ReadFile(Data.CriLev10Path))
            {
                case "12":
                    Lv10SteBut.interactable = true;
                    break;
                case "13":
                    Lv10ColBriBut.interactable = true;
                    break;
                case "123":
                case "132":
                    Lv10SteBut.interactable = true;
                    Lv10ColBriBut.interactable = true;
                    break;
            }
        }

        if (checkLv >= 10)
        {
            Lv10CplBut.interactable = true;
            Lv11Button.interactable = true;
            switch (Data.ReadFile(Data.CriLev11Path))
            {
                case "12":
                    Lv11SteBut.interactable = true;
                    break;
                case "13":
                    Lv11ColBriBut.interactable = true;
                    break;
                case "123":
                case "132":
                    Lv11SteBut.interactable = true;
                    Lv11ColBriBut.interactable = true;
                    break;
            }
        }

        if (checkLv >= 11)
        {
            Lv11CplBut.interactable = true;
            Lv12Button.interactable = true;
            switch (Data.ReadFile(Data.CriLev12Path))
            {
                case "12":
                    Lv12SteBut.interactable = true;
                    break;
                case "13":
                    Lv12ColBriBut.interactable = true;
                    break;
                case "123":
                case "132":
                    Lv12SteBut.interactable = true;
                    Lv12ColBriBut.interactable = true;
                    break;
            }
        }

        if (checkLv >= 12) Lv12CplBut.interactable = true;
    }

    public void HomeButton()
    {
        Application.LoadLevel("MainMenu");
    }

    public void Back()
    {
        Application.LoadLevel("Choose Chapter");
    }

    public void Level9Button()
    {
        Application.LoadLevel("Level 9");
    }

    public void Level10Button()
    {
        Application.LoadLevel("Level 10");
    }

    public void Level11Button()
    {
        Application.LoadLevel("Level 11");
    }

    public void Level12Button()
    {
        Application.LoadLevel("Level 12");
    }
}