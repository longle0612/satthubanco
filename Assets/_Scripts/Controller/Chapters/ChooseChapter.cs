using System;
using UnityEngine;
using UnityEngine.UI;

public class ChooseChapter : MonoBehaviour
{
    public static Text scoreText;
    private Button Chap2But;
    private Button Chap3But;
    [SerializeField] private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        Chap2But = helpers.FindButton("Chapter 2 Button");
        Chap3But = helpers.FindButton("Chapter 3 Button");
    }

    // Update is called once per frame
    void Update()
    {
        helpers.CheckMute(audioSource);
        scoreText.text = Data.ReadFile(Data.ScorePath);
        if (Convert.ToInt32(scoreText.text) >= 4) Chap2But.interactable = true;
        if (Convert.ToInt32(scoreText.text) >= 13) Chap3But.interactable = true;
    }

    public void HomeButton()
    {
        Application.LoadLevel("MainMenu");
    }

    public void ChooseChapter1()
    {
        Application.LoadLevel("Chapter 1");
    }

    public void ChooseChapter2()
    {
        Application.LoadLevel("Chapter 2");
    }

    public void ChooseChapter3()
    {
        Application.LoadLevel("Chapter 3");
    }
}