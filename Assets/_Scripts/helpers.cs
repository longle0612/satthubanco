using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class helpers : MonoBehaviour
{
    public static string IsInput()
    {
        if (Input.GetKeyDown(KeyCode.W)) return "W";
        if (Input.GetKeyDown(KeyCode.UpArrow)) return "UpArrow";
        if (Input.GetKeyDown(KeyCode.A)) return "A";
        if (Input.GetKeyDown(KeyCode.LeftArrow)) return "LeftArrow";
        if (Input.GetKeyDown(KeyCode.S)) return "S";
        if (Input.GetKeyDown(KeyCode.DownArrow)) return "DownArrow";
        if (Input.GetKeyDown(KeyCode.D)) return "D";
        if (Input.GetKeyDown(KeyCode.RightArrow)) return "RightArrow";
        return "";
    }

    public static void ChangePositionY(GameObject gameObj, Node node)
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.UpArrow) ||
            Input.GetKeyDown(KeyCode.DownArrow))
            gameObj.transform.position = CreateVector3(node);
    }

    public static void ChangePositionX(GameObject gameObj, Node node)
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.LeftArrow) ||
            Input.GetKeyDown(KeyCode.RightArrow))
            gameObj.transform.position = CreateVector3(node);
    }

    public static void FlChangePos(GameObject gameObj, Node node)
    {
        gameObj.transform.position = CreateVector3(node);
    }

    public static Vector3 CreateVector3(Node node)
    {
        return new Vector3(node.X, node.Y, node.Z);
    }

    // Return a button on request
    public static Button FindButton(string request)
    {
        return GameObject.Find(request).GetComponent<Button>();
    }

    public static void CheckMute(AudioSource audioSource)
    {
        switch (File.ReadAllText(Data.MutePath))
        {
            case "true":
                audioSource.mute = true;
                break;
            case "false":
                audioSource.mute = false;
                break;
        }
    }
}